import { IRootThunkDispatch } from '../store';
import { getUserData, getUserDetail } from './action';
import Swal from 'sweetalert2';

const { REACT_APP_API_SERVER } = process.env

function failAlert(msg: string){
  Swal.fire({
    icon: 'error',
    text: msg,
  })
  return
}


export function getUserDataThunk(){
  
  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getUserData`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
    });
    const result = await res.json()
    let userData = []
    for (let i = 0; i < result.length; i++){
      let object = {
        userID: result[i].id,
        username: result[i].username,
        email: result[i].email,
        coins: result[i].coins,
        lastOnline: (new Date(result[i].last_fetch_time)).toDateString().slice(11) + " " + (new Date(result[i].last_fetch_time)).toDateString().slice(4, 10) + " " + (new Date(result[i].last_fetch_time)).toTimeString().slice(0, 8),
      }
      userData.push(object)
    }

    dispatch(getUserData(userData))
  }
}

export function getUserDetailThunk(userID: number){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getUserDetail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ userID: userID })
    });
    const result = await res.json()
    let detail = {
      ...result, 
      birthday: (new Date(result.birthday)).toDateString().slice(11) + " " + (new Date(result.birthday)).toDateString().slice(4, 10),
      created_at: (new Date(result.created_at)).toDateString().slice(11) + " " + (new Date(result.created_at)).toDateString().slice(4, 10) + " " + (new Date(result.created_at)).toTimeString().slice(0, 8),
    }
    
    dispatch(getUserDetail(detail))
  }
}

export function editMultiUsernameThunk(arrayToServer: number[],array: number[], newName: string, status?: string){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/editMultiUsername`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ userIDs: arrayToServer, newName: newName })
    });
    if (res.status !== 200){
      failAlert("Fail to update multi username")
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Saved Changes.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getUserDataThunk())
        if (status === "userDetail"){
          dispatch(getUserDetailThunk(arrayToServer[0]))
        }
      }, 1500)
    }
  }
}

export function updateMultiCoinThunk(arrayToServer: number[], array: number[], newCoin: number, description: string, status?: string){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/updateMultiCoin`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ userIDs: arrayToServer, newCoin: newCoin, description: description })
    });
    const result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Saved Changes.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getUserDataThunk())
        if (status === "userDetail"){
          dispatch(getUserDetailThunk(arrayToServer[0]))
        }
      }, 1500)
    }
  }
}

export function deleteMultiUserThunk(arrayToServer: number[]){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/deleteMultiUser`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ userIDs: arrayToServer })
    });
    const result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Saved Changes.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getUserDataThunk())
      }, 1500)
    }
  }
}

export function blockUserThunk(userID: number){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/blockUser`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ userID: userID })
    });
    let result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Blocked.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getUserDetailThunk(userID))
      }, 1500)
    }
  }
}

export function unblockUserThunk(userID: number){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/unblockUser`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ userID: userID })
    });
    let result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'User is unblocked.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getUserDetailThunk(userID))
      }, 1500)
    }
  }
}

export function removeProfilePicThunk(id: number, url: string){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/removeProfilePic`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ userID: id, url: url })
    });
    let result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Profile picture is removed.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getUserDetailThunk(id))
      }, 1500)
    }
  }
}

export function editUserDetailThunk(content:{
  phone_num: string | null, 
  registration_status: number | null, 
  birthday: string | null, 
  gender: string | null,
  text_description: string | null,
  voice_description: string | null
}, userID: number){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/editUserDetail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ content: content, userID: userID })
    });
    let result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'User detail is edited.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getUserDetailThunk(userID))
      }, 1500)
    }
  }
}

export function deletePhotoThunk(url: string, userID: number){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/deletePhoto`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ url: url, userID: userID })
    });
    let result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'User photo is deleted.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getUserDetailThunk(userID))
      }, 1500)
    }
  }
}