export type IUserState = {
  userData: {
    userID: number,
    username: string,
    email: string,
    coins: number,
    lastOnline: string,
  }[],
  userSearchType: "userID" | "username" | "email",
  userSearchKeyword: string,
  userDataAfterSearch: {
    userID: number,
    username: string,
    email: string,
    coins: number,
    lastOnline: string,
  }[],
  userDetail: {
    id: number;
    username: string | null,
    email: string | null,
    coins: number | null,
    last_fetch_time: string | null,
    phone_num: string | null;
    registration_status: number;
    google_access_token: string | null;
    birthday: string | null;
    gender: string | null;
    created_at: string;
    text_description: string | null;
    voice_description: string | null;
    isBlocked: boolean;
    isFake: boolean;
    user_photos: {
      id: number | null,
      url: string | null,
      description: string | null,
      created_at: string | null
    }[];
    user_transactions: {
      id: number | null,
      transaction: number | null,
      description: string | null,
      created_by: string | null,
      created_at: string | null
    }[];
    userLike: {
      friend_id: number | null,
      username: string | null,
      email: string | null, 
      match_coordinates: {x: number | null; y: number | null},
      created_at: string | null,
      isScheduled: boolean | null,
    }[];
    userBeLiked: {
      user_id: number | null,
      username: string | null,
      email: string | null, 
      match_coordinates: {x: number | null; y: number | null},
      created_at: string | null,
      isScheduled: boolean | null,
    }[];
    matched: {
      friend_id: number | null,
      username: string | null,
      email: string | null, 
      user_match_coordinates: {x: number | null; y: number | null},
      friend_match_coordinates: {x: number | null; y: number | null},
      user_like_time: string | null,
      friend_like_time: string | null,
      isScheduled: boolean | null,
    }[];
    userTags: {
      tag: string | null,
      class: string | null,
      created_at: string | null,
    }[];
    userWishTags: {
      tag: string | null,
      class: string | null,
      created_at: string | null,
    }[];
    schedule: {
      id: number | null,
      inviter: string | null,
      inviter_id: number | null,
      invitee: string | null,
      invitee_id: number | null,
      is_accepted: boolean | null,
      scheduled_at: string | null,
      created_at: string | null,
    }[];
    model_accuracy: number,
  }
}

export const initialState: IUserState = {
  userData: [{
    userID: 0,
    username: "username",
    email: "email",
    coins: 0,
    lastOnline: "lastOnline",
  }],
  userSearchType: "userID",
  userSearchKeyword: "",
  userDataAfterSearch: [{
    userID: 0,
    username: "username",
    email: "email",
    coins: 0,
    lastOnline: "lastOnline",
  }],
  userDetail: {
    id: 0,
    username: null,
    email: null,
    coins: null,
    last_fetch_time: null,
    phone_num: null,
    registration_status: -999,
    google_access_token: null,
    birthday: null,
    gender: null,
    created_at: "000 00 0000",
    text_description: null,
    voice_description: null,
    isBlocked: false,
    isFake: false,
    user_photos: [{
      id: null,
      url: null,
      description: null,
      created_at: null
    }],
    user_transactions: [{
      id: null,
      transaction: null,
      description: null,
      created_by: null,
      created_at: null
    }],
    userLike: [{
      friend_id: null,
      username: null,
      email: null, 
      match_coordinates: {x: null, y: null},
      created_at: null,
      isScheduled: null,
    }],
    userBeLiked: [{
      user_id: null,
      username: null,
      email: null, 
      match_coordinates: {x: null, y: null},
      created_at: null,
      isScheduled: null,
    }],
    matched: [{
      friend_id: null,
      username: null,
      email: null, 
      user_match_coordinates: {x: null, y: null},
      friend_match_coordinates: {x: null, y: null},
      user_like_time: null,
      friend_like_time: null,
      isScheduled: null,
    }],
    userTags: [{
      tag: null,
      class: null,
      created_at: null,
    }],
    userWishTags: [{
      tag: null,
      class: null,
      created_at: null,
    }],
    schedule: [{
      id: null,
      inviter: null,
      inviter_id: null,
      invitee: null,
      invitee_id: null,
      is_accepted: null,
      scheduled_at: null,
      created_at: null,
    }],
    model_accuracy: 0,
  }
}