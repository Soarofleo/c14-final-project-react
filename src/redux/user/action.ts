export function changeUserSearchType(search: "userID" | "username" | "email"){
  return {
    type: '@@User/change-search-type' as const,
    search
  }
}

export function getUserData(userData: any){
  return {
    type: '@@User/get-data' as const,
    userData
  }
}

export function handleUserDataAfterSearch(keyword: string){
  return {
    type: '@@User/handle-data-after-search' as const,
    keyword
  }
}

export function getUserDetail(userDetail: any){
  return {
    type: '@@User/get-user-detail' as const,
    userDetail
  }
}

export function editMultiUsername(array: number[], newName: string){
  return {
    type: '@@User/edit-multi-username' as const,
    array,
    newName
  }
}

export function updateMultiCoin(array: number[], newCoin: number){
  return {
    type: '@@User/give-multi-coin' as const,
    array,
    newCoin
  }
}

export function deleteMultiUser(array: number[]){
  return {
    type: '@@User/delete-multi-user' as const,
    array
  }
}

export type IUserAction = 
  | ReturnType<typeof changeUserSearchType>
  | ReturnType<typeof getUserData>
  | ReturnType<typeof handleUserDataAfterSearch>
  | ReturnType<typeof getUserDetail>
  | ReturnType<typeof editMultiUsername>
  | ReturnType<typeof updateMultiCoin>
  | ReturnType<typeof deleteMultiUser>
