import produce from 'immer';
import {IDashboardAction} from './action';
import {IDashBoardState, initialState} from './state';

export const DashboardReducer = produce((
  state: IDashBoardState = initialState,
  action: IDashboardAction,
): void=>{
  switch (action.type){

    case '@@Dashboard/change-type':{
      try {
        state.chartType = action.chart
        return
      } catch (error) {
        console.error(error)
        return
      }
    }
    
    case '@@Dashboard/change-period':{
      try {
        state.period = action.period
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

    case '@@Dashboard/get-data':{
      try {
        state.statData = {...action.data}
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

    case '@@Dashboard/change-data-type':{
      try {
        state.statDataType = action.dataType
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

    case '@@Dashboard/update-user-analyst-data':{
      try {
        state.userData = action.data
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

    case '@@Dashboard/change-user-data-type':{
      try {
        state.userDataType = action.dataType
        state.filter = []
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

    case '@@Dashboard/set-filter':{
      try {
        state.filter = action.array
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

    case '@@Dashboard/get-sub-data':{
      try {
        state.subData = action.array
        state.subDataMode = true
        state.subDataType = action.subDataType
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

    case '@@Dashboard/get-sub-data-for-place':{
      try {
        state.subDataForPlace = action.array
        state.subDataMode = true
        state.subDataType = action.subDataType
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

    case '@@Dashboard/open-sub-data-mode':{
      try {
        state.subDataMode = true
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

    case '@@Dashboard/close-sub-data-mode':{
      try {
        state.subDataMode = false
        return
      } catch (error) {
        console.error(error)
        return
      }
    }

  }
}, initialState)