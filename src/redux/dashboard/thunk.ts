import { IRootThunkDispatch } from '../store';
import { changeUserDataType, getData, getSubData, getSubDataForPlace, updateUserAnalystData } from './action';
const { REACT_APP_API_SERVER } = process.env

export function getDataThunk(){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getData`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
    });
    const result = await res.json()
    
    let dailyUser = []
    let dailyRegistration = []
    let dailyMatch = []
    let dailyConsumption = []
    let data;
    try {
      for (let i = 0; i < result.dailyRegistration.length; i++){
        dailyRegistration.push(result.dailyRegistration[i])
        if (i < result.dailyConsumption.length){
          dailyConsumption.push(result.dailyConsumption[i])
        }
        if (i < result.dailyUser.length){
          dailyUser.push(result.dailyUser[i])
        }
        if (i < result.dailyMatch.length){
          dailyMatch.push(result.dailyMatch[i])
        }
      }
      if (dailyRegistration.length < 365){
        for (let i = dailyRegistration.length; i < 365; i++){
          dailyRegistration.push(0)
        }
        for (let i = dailyConsumption.length; i < 365; i++){
          dailyConsumption.push(0)
        }
        for (let i = dailyUser.length; i < 365; i++){
          dailyUser.push(0)
        }
        for (let i = dailyMatch.length; i < 365; i++){
          dailyMatch.push(0)
        }
      }
      data = {
        dailyUser: dailyUser,
        dailyRegistration: dailyRegistration,
        dailyMatch: dailyMatch,
        dailyConsumption: dailyConsumption
      }
    } catch {
      data = {
        dailyUser: [1234, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1000, 900, 1000, 800, 1000],
        dailyRegistration: [123, 100, 80, 120, 100, 90, 60, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1000, 900, 1000, 800, 1000],
        dailyMatch: [345, 300, 200, 250, 200, 240, 160, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1000, 900, 1000, 800, 1000],
        dailyConsumption: [3450, 1500, 2500, 2000, 2400, 1500, 2000, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 600, 1000, 800, 1200, 1000, 900, 1000, 800, 1000, 900, 1000, 800, 1000],
      }
    }

    dispatch(getData(data))
  }
}


export function getUserTagAnalystDataThunk(){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getUserTagAnalystData`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
    });
    const result = await res.json()
    let data = [];
    for (let i = 0; i < result.data.length; i++){
      if (result.data[i].name === "習慣1"){
        data.push({name: "吸煙習慣", value: result.data[i].value})
      } else if (result.data[i].name === "習慣2"){
        data.push({name: "飲酒習慣", value: result.data[i].value})
      } else {
        data.push(result.data[i])
      }
    }
    dispatch(updateUserAnalystData(data))
  }
}

export function getUserWishTagAnalystDataThunk(){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getUserWishTagAnalystData`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
    });
    const result = await res.json()
    let data = [];
    for (let i = 0; i < result.data.length; i++){
      if (result.data[i].name === "習慣1"){
        data.push({name: "吸煙習慣", value: result.data[i].value})
      } else if (result.data[i].name === "習慣2"){
        data.push({name: "飲酒習慣", value: result.data[i].value})
      } else {
        data.push(result.data[i])
      }
    }
    dispatch(updateUserAnalystData(data))
  }
}

export function getUserGenderAnalystDataThunk(){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getUserGenderAnalystData`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
    });
    const result = await res.json()
    let data = result.data;
    for (let i = 0; i < result.data.length; i++){
      if (result.data[i].name === "其他"){
        data[i].name = "other"
      }
    }
    dispatch(updateUserAnalystData(data))
  }
}

export function getUserAgeAnalystDataThunk(){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getUserAgeAnalystData`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
    });
    const result = await res.json()
    let data;
    let genderData = [
      {name: "18-21", value: 0},
      {name: "22-25", value: 0},
      {name: "26-29", value: 0},
      {name: "30-34", value: 0},
      {name: "35-39", value: 0},
      {name: "40-49", value: 0},
      {name: "50-59", value: 0},
      {name: "60+", value: 0},
    ]
    for (let i = 0; i < result.data.length; i++){
      if (result.data[i].name >= (new Date().getFullYear() - 21)){
        genderData[0].value = genderData[0].value + parseInt(result.data[i].value)
      } else if (result.data[i].name >= (new Date().getFullYear() - 25)) {
        genderData[1].value = genderData[1].value + parseInt(result.data[i].value)
      } else if (result.data[i].name >= (new Date().getFullYear() - 29)) {
        genderData[2].value = genderData[2].value + parseInt(result.data[i].value)
      } else if (result.data[i].name >= (new Date().getFullYear() - 34)) {
        genderData[3].value = genderData[3].value + parseInt(result.data[i].value)
      } else if (result.data[i].name >= (new Date().getFullYear() - 39)) {
        genderData[4].value = genderData[4].value + parseInt(result.data[i].value)
      } else if (result.data[i].name >= (new Date().getFullYear() - 49)) {
        genderData[5].value = genderData[5].value + parseInt(result.data[i].value)
      } else if (result.data[i].name >= (new Date().getFullYear() - 59)) {
        genderData[6].value = genderData[6].value + parseInt(result.data[i].value)
      } else {
        genderData[7].value = genderData[7].value + parseInt(result.data[i].value)
      }
    }
    data = genderData
    dispatch(updateUserAnalystData(data))
  }
}

export function getUserPlaceAnalystDataThunk(){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getPlacesAnalystData`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
    });
    const result = await res.json()
    let data = result.data;
    dispatch(updateUserAnalystData(data))
  }
}

export function changeUserDataTypeThunk(title: "userTag" | "userWishTag" | "gender" | "age" | "place"){
  return async (dispatch: IRootThunkDispatch) => {
    if (title === "userTag"){
      dispatch(getUserTagAnalystDataThunk())      
    } else if (title === "userWishTag"){
      dispatch(getUserWishTagAnalystDataThunk())
    } else if (title === "gender"){
      dispatch(getUserGenderAnalystDataThunk())
    } else if (title === "age"){
      dispatch(getUserAgeAnalystDataThunk())
    } else if (title === "place"){
      dispatch(getUserPlaceAnalystDataThunk())
    }
    dispatch(changeUserDataType(title))
  }
}

export function getSubDataThunk(type: string, title: string){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getSubData`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ type: type, title: title})
    });
    const result = await res.json()
    let data;
    let genderData = [
      {name: "18-21", value: 0},
      {name: "22-25", value: 0},
      {name: "26-29", value: 0},
      {name: "30-34", value: 0},
      {name: "35-39", value: 0},
      {name: "40-49", value: 0},
      {name: "50-59", value: 0},
      {name: "60+", value: 0},
    ]
    if (title === "gender"){
      for (let i = 0; i < result.data.length; i++){
        if (result.data[i].name >= (new Date().getFullYear() - 21)){
          genderData[0].value = genderData[0].value + parseInt(result.data[i].value)
        } else if (result.data[i].name >= (new Date().getFullYear() - 25)) {
          genderData[1].value = genderData[1].value + parseInt(result.data[i].value)
        } else if (result.data[i].name >= (new Date().getFullYear() - 29)) {
          genderData[2].value = genderData[2].value + parseInt(result.data[i].value)
        } else if (result.data[i].name >= (new Date().getFullYear() - 34)) {
          genderData[3].value = genderData[3].value + parseInt(result.data[i].value)
        } else if (result.data[i].name >= (new Date().getFullYear() - 39)) {
          genderData[4].value = genderData[4].value + parseInt(result.data[i].value)
        } else if (result.data[i].name >= (new Date().getFullYear() - 49)) {
          genderData[5].value = genderData[5].value + parseInt(result.data[i].value)
        } else if (result.data[i].name >= (new Date().getFullYear() - 59)) {
          genderData[6].value = genderData[6].value + parseInt(result.data[i].value)
        } else {
          genderData[7].value = genderData[7].value + parseInt(result.data[i].value)
        }
      }
      data = genderData
    } else {
      data = result.data
    }
    if (title === 'place'){
      data.sort((a: any, b: any) => {return b.count - a.count})
      dispatch(getSubDataForPlace(data, type))
    } else {
      dispatch(getSubData(data, type))
    }
  }
}