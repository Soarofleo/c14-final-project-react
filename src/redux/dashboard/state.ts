export type IDashBoardState = {
  statData: {
    dailyUser: number[] | null[], 
    dailyRegistration: number[] | null[], 
    dailyMatch: number[] | null[], 
    dailyConsumption: number[] | null[]},
  chartType: "Bar" | "Spline" | "Pie",
  period: number,
  statDataType: "dailyUser" | "dailyRegistration" | "dailyMatch" | "dailyConsumption",
  userData: { name: string, value: number }[],
  userDataType: "userTag" | "userWishTag" | "gender" | "age" | "place",
  subData: { name: string, value: number }[],
  subDataForPlace: { 
    id: number, 
    count: string, 
    location: {x: number, y: number}, 
    name: 'string', 
    place_id: string,
    vicinity: string
  }[],
  subDataMode: boolean,
  subDataType: string,
  filter: { name: string, value: number }[],
}

export const initialState: IDashBoardState = {
  statData: {
    dailyUser: [null], 
    dailyRegistration: [null], 
    dailyMatch: [null], 
    dailyConsumption: [null]},
  chartType: "Bar",
  period: 7,
  statDataType: "dailyUser",
  userData: [],
  userDataType: "userTag",
  subData: [],
  subDataForPlace: [],
  subDataMode: false,
  subDataType: "",
  filter: [],
}