export function changeType(chart: "Bar" | "Spline" | "Pie"){
  return {
    type: '@@Dashboard/change-type' as const,
    chart
  }
}

export function changePeriod(period: number){
  return {
    type: '@@Dashboard/change-period' as const,
    period
  }
}

export function getData(data: any){
  return {
    type: '@@Dashboard/get-data' as const,
    data
  }
}

export function changeDataType(dataType: "dailyUser" | "dailyRegistration" | "dailyMatch" | "dailyConsumption"){
  return {
    type: '@@Dashboard/change-data-type' as const,
    dataType
  }
}

export function updateUserAnalystData(data: any){
  return {
    type: '@@Dashboard/update-user-analyst-data' as const,
    data
  }
}

export function changeUserDataType(dataType: "userTag" | "userWishTag" | "gender" | "age" | "place"){
  return {
    type: '@@Dashboard/change-user-data-type' as const,
    dataType
  }
}

export function setFilter(array: { name: string, value: number }[]){
  return {
    type: '@@Dashboard/set-filter' as const,
    array
  }
}

export function getSubData(array: { name: string, value: number }[], subDataType: string){
  return {
    type: '@@Dashboard/get-sub-data' as const,
    array,
    subDataType,
  }
}

export function getSubDataForPlace(array: { 
  id: number, 
  count: string, 
  location: {x: number, y: number}, 
  name: 'string', 
  place_id: string,
  vicinity: string
}[], subDataType: string){
  return {
    type: '@@Dashboard/get-sub-data-for-place' as const,
    array,
    subDataType,
  }
}

export function openSubDataMode(){
  return {
    type: '@@Dashboard/open-sub-data-mode' as const,
  }
}

export function closeSubDataMode(){
  return {
    type: '@@Dashboard/close-sub-data-mode' as const,
  }
}

export type IDashboardAction = 
  | ReturnType<typeof changeType>
  | ReturnType<typeof changePeriod>
  | ReturnType<typeof getData>
  | ReturnType<typeof changeDataType>
  | ReturnType<typeof updateUserAnalystData>
  | ReturnType<typeof changeUserDataType>
  | ReturnType<typeof setFilter>
  | ReturnType<typeof getSubData>
  | ReturnType<typeof getSubDataForPlace>
  | ReturnType<typeof openSubDataMode>
  | ReturnType<typeof closeSubDataMode>