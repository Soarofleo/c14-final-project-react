import {createStore, combineReducers, compose, applyMiddleware, CombinedState, AnyAction } from "redux";
import logger from 'redux-logger';
import {
  RouterState,
  connectRouter,
  routerMiddleware,
  CallHistoryMethodAction
} from 'connected-react-router';
import { createBrowserHistory } from 'history';
import thunk, { ThunkDispatch } from 'redux-thunk'
import { IDashBoardState } from "./dashboard/state";
import { DashboardReducer } from "./dashboard/reducer";
import { IDashboardAction } from "./dashboard/action";
import { IUserAction } from "./user/action";
import { UserReducer } from "./user/reducer";
import { IUserState } from "./user/state";
import { ReportReducer } from "./report/reducer";
import { IReportAction } from "./report/action";
import { IReportState } from "./report/state";
import { IAdminState } from "./admin/state";
import { IAdminAction } from "./admin/action";
import { AdminReducer } from "./admin/reducer";
import { ILoginState } from "./login/state";
import { ILoginAction } from "./login/action";
import { LoginReducer } from "./login/reducer";
import { IConsoleState } from "./console/state";
import { IConsoleAction } from "./console/action";
import { ConsoleReducer } from "./console/reducer";

export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>

export const history = createBrowserHistory();

export type IRootState = {
  dashboard: IDashBoardState
  report: IReportState
  user: IUserState
  admin: IAdminState
  login: ILoginState
  console: IConsoleState
  router: RouterState
}

export type IRootAction = IDashboardAction | IReportAction | IUserAction | IAdminAction | ILoginAction | IConsoleAction | CallHistoryMethodAction;

export const appReducer = combineReducers<IRootState>({
  dashboard: DashboardReducer,
  report: ReportReducer,
  user: UserReducer,
  admin: AdminReducer,
  login: LoginReducer,
  console: ConsoleReducer,
  router: connectRouter(history)
})

const rootReducer = (
  state: CombinedState<IRootState> | undefined,
  action: AnyAction,
) => {
  if (action.type === '@@Login/logout') {
    state = undefined;
  }

  return appReducer(state, action);
};
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore<IRootState,IRootAction,{},{}>
  (
    rootReducer, 
    composeEnhancers(
      applyMiddleware(logger),
      applyMiddleware(routerMiddleware(history)),
      applyMiddleware(thunk),
    )
  );
export default store;