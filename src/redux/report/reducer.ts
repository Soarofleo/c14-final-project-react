import produce from 'immer';
import {IReportAction} from './action';
import {IReportState, initialState} from './state';

function updateSearchResult(state: IReportState, keyword: string){
  let newArray: {
    reportID: number,
    reporterID: number,
    reporter: string,
    email: string,
    category: string,
    targetID: number,
    content: string,
    isRead: string,
    isReplied: string,
    replied_by: string,
    date: string
  }[] = [];
  for (let i = 0; i < state.reportData.length; i++){
    let type = state.reportSearchType
    let target: any = state.reportData[i][type]?.toString()
    if (target.toUpperCase().includes(keyword.toUpperCase())){
      let item = {
        reportID: state.reportData[i]["reportID"],
        reporterID: state.reportData[i]["reporterID"],
        reporter: state.reportData[i]["reporter"],
        email: state.reportData[i]["email"],
        category: state.reportData[i]["category"],
        targetID: state.reportData[i]["targetID"],
        content: state.reportData[i]["content"],
        isRead: state.reportData[i]["isRead"],
        isReplied: state.reportData[i]["isReplied"],
        replied_by: state.reportData[i]["replied_by"],
        date: state.reportData[i]["date"]
      }
      newArray.push(item)
    }
  }
  state.reportDataAfterSearch = newArray.slice()
  return
}

export const ReportReducer = produce((
  state: IReportState = initialState,
  action: IReportAction,
): void =>{
  switch (action.type){

    case '@@Report/change-search-type':{
      try {
        state.reportSearchType = action.search
        return
      } catch (error) {

        return
      }
    }
    
    case '@@Report/get-data':{
      try {
        state.reportData = action.reportData.slice()
        return
      } catch (error) {

        return
      }
    }

    case '@@Report/handle-data-after-search':{
      try {
        state.reportSearchKeyword = action.keyword
        updateSearchResult(state, action.keyword)
        return
      } catch (error) {

        return
      }
    }

    case '@@Report/update-replied-status':{
      try {
        if (action.isReplied === "no"){
          state.reportData[action.id - 1].isReplied = "yes"
        } else {
          state.reportData[action.id - 1].isReplied = "no"
        }
        return
      } catch (error) {

        return
      }
    }

    case '@@Report/update-multi-replied-status':{
      try {
        for (let i = 0; i < action.array.length; i++){
          state.reportData[action.array[i] - 1].isReplied = "yes"
        }
        return
      } catch (error) {

        return
      }
    }

    case '@@Report/restore-multi-replied-status':{
      try {
        for (let i = 0; i < action.array.length; i++){
          state.reportData[action.array[i] - 1].isReplied = "no"
        }
        return
      } catch (error) {

        return
      }
    }

    case '@@Report/update-read-status':{
      try {
        state.reportData[action.id - 1].isRead = "yes"
        return
      } catch (error) {

        return
      }
    }

  }
}, initialState)