export type IReportState = {
  reportData: {
    reportID: number,
    reporterID: number,
    reporter: string,
    email: string,
    category: string,
    targetID: number,
    content: string,
    isRead: string,
    isReplied: string,
    replied_by: string,
    date: string
  }[],
  reportSearchType: "reportID" | "email" | "category" | "content",
  reportSearchKeyword: string,
  reportDataAfterSearch: {
    reportID: number,
    reporterID: number,
    reporter: string,
    email: string,
    category: string,
    targetID: number,
    content: string,
    isRead: string,
    isReplied: string,
    replied_by: string,
    date: string
  }[],
  // unreadReportCounter: number
}

export const initialState: IReportState = {
  reportData: [{
    reportID: 0,
    reporterID: 0,
    reporter: "reporter",
    email: "email",
    category: "category",
    targetID: 0,
    content: "content",
    isRead: "no",
    isReplied: "no",
    replied_by: "no",
    date: "date"
  }],
  reportSearchType: "reportID",
  reportSearchKeyword: "",
  reportDataAfterSearch: [{
    reportID: 0,
    reporterID: 0,
    reporter: "reporter",
    email: "email",
    category: "category",
    targetID: 0,
    content: "content",
    isRead: "no",
    isReplied: "no",
    replied_by: "no",
    date: "date"
  }],
  // unreadReportCounter: 0
}