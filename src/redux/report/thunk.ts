import { IRootThunkDispatch } from '../store';
import { getReportData } from './action';

const { REACT_APP_API_SERVER } = process.env

export function getReportDataThunk(){
  
  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getReportData`, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
    });
    const result = await res.json()
    let reportData = []
    for (let i = 0; i < result.length; i++){
      let object = {
        reportID: result[i].id,
        reporterID: result[i].user_id,
        reporter: result[i].username,
        email: result[i].email,
        category: result[i].category,
        targetID: result[i].target_id,
        content: result[i].content,
        isRead: `${result[i].read === true ? "yes" : "no"}`,
        isReplied: `${result[i].replied === true ? "yes" : "no"}`,
        replied_by: `${result[i].replied_by}`,
        date: (new Date(result[i].created_at)).toDateString().slice(11) + " " + (new Date(result[i].created_at)).toDateString().slice(4, 10) + " " + (new Date(result[i].created_at)).toTimeString().slice(0, 8),
      }
      reportData.push(object)
    }
    dispatch(getReportData(reportData))
  }
}

export function updateRepliedStatusThunk(isReplied: string, id: number){

  return async (dispatch: IRootThunkDispatch) => {
    await fetch(`${REACT_APP_API_SERVER}/updateReplyStatus`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ reply: isReplied, report_id: id })
    });
    dispatch(getReportDataThunk())
  }
}

export function updateMultiRepliedStatusThunk(array: number[]){

  return async (dispatch: IRootThunkDispatch) => {
    await fetch(`${REACT_APP_API_SERVER}/updateMultiReplyStatus`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ report_ids: array })
    });
    dispatch(getReportDataThunk())
  }
}

export function restoreMultiRepliedStatusThunk(array: number[]){

  return async (dispatch: IRootThunkDispatch) => {
    await fetch(`${REACT_APP_API_SERVER}/restoreMultiReplyStatus`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ report_ids: array })
    });
    dispatch(getReportDataThunk())
  }
}

export function updateReadStatusThunk(id: number){

  return async (dispatch: IRootThunkDispatch) => {
    await fetch(`${REACT_APP_API_SERVER}/updateReadStatus`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ report_id: id })
    });
    dispatch(getReportDataThunk())
  }
}