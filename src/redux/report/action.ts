export function changeReportSearchType(search: "reportID" | "email" | "category" | "content"){
  return {
    type: '@@Report/change-search-type' as const,
    search
  }
}

export function getReportData(reportData: any){
  return {
    type: '@@Report/get-data' as const,
    reportData
  }
}

export function handleReportDataAfterSearch(keyword: string){
  return {
    type: '@@Report/handle-data-after-search' as const,
    keyword
  }
}

export function updateRepliedStatus(isReplied: string, id: number){
  return {
    type: '@@Report/update-replied-status' as const,
    isReplied,
    id,
  }
}

export function updateMultiRepliedStatus(array: number[]){
  return {
    type: '@@Report/update-multi-replied-status' as const,
    array,
  }
}

export function restoreMultiRepliedStatus(array: number[]){
  return {
    type: '@@Report/restore-multi-replied-status' as const,
    array,
  }
}

export function updateReadStatus(id: number){
  return {
    type: '@@Report/update-read-status' as const,
    id,
  }
}

export type IReportAction = 
| ReturnType<typeof changeReportSearchType>
| ReturnType<typeof getReportData>
| ReturnType<typeof handleReportDataAfterSearch>
| ReturnType<typeof updateRepliedStatus>
| ReturnType<typeof updateMultiRepliedStatus>
| ReturnType<typeof restoreMultiRepliedStatus>
| ReturnType<typeof updateReadStatus>
