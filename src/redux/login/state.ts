export type ILoginState = {isLogin: false} | {isLogin: true, currentAdmin: ILoginData, token: JWTPayload}

export type ILoginData = {
  adminID: number,
  adminName: string,
  email: string,
  role: "Super" | "Ordinary" | "Intern",
  theme: "lightBlue" | "crimson" | "light" | "dark" | "darkMode" 
}

export type JWTPayload = {
  adminID: number,
}

export const initialState: ILoginState = {
  isLogin: false,
}