import {ILoginAction} from './action';
import {ILoginState, initialState, JWTPayload} from './state';
import jwt_decode from 'jwt-decode'

export const LoginReducer = (
  state: ILoginState = initialState,
  action: ILoginAction,
): ILoginState => {
  switch (action.type){

    case '@@Login/get-current-admin':{
      try{
        let payload: JWTPayload = jwt_decode(action.token)
        return {
          isLogin: true,
          currentAdmin: action.currentAdmin,
          token: payload
        }
      }catch(error){

        return state
      }
    }

    case '@@Login/logout':{
      try{
        localStorage.removeItem('token')
        return {isLogin: false}
      }catch(error){

        return state
      }
    }

    case '@@Login/update-theme':{
      try{
        if (state.isLogin === true){
          return {...state, currentAdmin: {...state.currentAdmin, theme: action.theme}}
        } else {
          return state
        }
      } catch (error) {

        return state
      }
    }

    case '@@Login/load_token':{
      try{
        let payload: JWTPayload = jwt_decode(action.token)
        return {...state, isLogin: true, currentAdmin: action.data, token: payload}
      } catch (error) {

        return state
      }
    }

    default: return state
  }
}