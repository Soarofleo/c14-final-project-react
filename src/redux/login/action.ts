export function getCurrentAdmin(currentAdmin: {
  adminID: number,
  adminName: string,
  email: string,
  role: "Super" | "Ordinary" | "Intern",
  theme: "lightBlue" | "crimson" | "light" | "dark" | "darkMode"
}, token: string){
  return {
    type: '@@Login/get-current-admin' as const,
    currentAdmin,
    token
  }
}

export function logout(){
  return {
    type: '@@Login/logout' as const,
  }
}

export function updateTheme(theme: "lightBlue" | "crimson" | "light" | "dark" | "darkMode"){
  return {
    type: '@@Login/update-theme' as const,
    theme
  }
}

export function loadToken(token: string, data: {
  adminID: number,
  adminName: string,
  email: string,
  role: "Super" | "Ordinary" | "Intern",
  theme: "lightBlue" | "crimson" | "light" | "dark" | "darkMode"
}) {
  return {
      type: '@@Login/load_token' as const,
      token,
      data
  }
}

export type ILoginAction = 
  | ReturnType<typeof getCurrentAdmin>
  | ReturnType<typeof logout>
  | ReturnType<typeof updateTheme>
  | ReturnType<typeof loadToken>