import { IRootThunkDispatch } from '../store';
import { getCurrentAdmin, loadToken } from './action';
import Swal from 'sweetalert2'

const { REACT_APP_API_SERVER } = process.env

export function loginThunk(email: string, password: string){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/adminLogin`, {
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({ email: email, password: password })
    });
    const check: any = await res.json()
    if (check.msg){
      Swal.fire({
        title: 'Error!',
        text: 'Incorrect email address or password',
        icon: 'error',
        confirmButtonText: 'OK'
      })
      return
    } else {
      let token = check["data"]["token"]
      localStorage.setItem('token', token)
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Login success.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(loadToken(token, check["data"]["admin_data"]))
      }, 1500)
    }

  }
}

export function loadTokenThunk(){

  return async (dispatch: IRootThunkDispatch) => {
    let token = localStorage.getItem('token')
    if(token && token !== "undefined"){
      const res = await fetch(`${REACT_APP_API_SERVER}/getCurrentAdmin`, {
        headers: {
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": ("Bearer " + token)
        },
      });
      let check = await res.json()
      if (check.msg){
        Swal.fire({
          title: 'Error!',
          text: 'Incorrect token! Please login again.',
          icon: 'error',
          confirmButtonText: 'OK'
        })
        return
      } else {
        dispatch(getCurrentAdmin(check["data"], token))
      }
    }
  }
}