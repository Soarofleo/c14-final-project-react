export function getAdminData(adminData: any){
  return {
    type: '@@Admin/get-data' as const,
    adminData,
  }
}

export function deleteMultiAdmin(array: number[]){
  return {
    type: '@@Admin/delete-multi-admin' as const,
    array
  }
}

export type IAdminAction = 
  | ReturnType<typeof getAdminData>
  | ReturnType<typeof deleteMultiAdmin>