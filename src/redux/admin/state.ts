export type IAdminState = {type: "loading"} | {type: "ready", data: IAdminData}
export type IAdminData = {
  adminData: {
    adminID: number,
    adminName: string,
    email: string,
    role: "Super" | "Ordinary" | "Intern",
    authority: "Super" | "Ordinary" | "Intern",
}[],
}

export const initialState: IAdminState = {
  type: "loading"
  // adminData: [{
  //   adminID: 0,
  //   adminName: "admin",
  //   email: "email",
  //   role: "Intern",
  //   authority: "Intern",
  // }],
  // currentAdmin: {
  //   adminID: 0,
  //   adminName: "admin",
  //   email: "email",
  //   role: "Intern",
  //   theme: "lightBlue"
  // },
}