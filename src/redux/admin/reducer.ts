import {IAdminAction} from './action';
import {IAdminState, initialState} from './state';


export const AdminReducer = (
  state: IAdminState = initialState,
  action: IAdminAction,
): IAdminState =>{
  switch (action.type){

    case '@@Admin/get-data':{
      return {type: "ready", data: action}
    }

    default: return state
  }
}