import { IRootThunkDispatch } from '../store';
import { getAdminData } from './action';
import Swal from 'sweetalert2';
import { updateTheme } from '../login/action';
import { loadTokenThunk } from '../login/thunk';

const { REACT_APP_API_SERVER } = process.env

function checkResult(result: string){
  if (result === "success"){
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Your password has been updated.',
      showConfirmButton: false,
      timer: 1500
    })
  } else if (result === "failed"){
    Swal.fire({
      icon: 'error',
      text: 'Something went wrong! Please try again.',
    })
  }
}

function failAlert(msg: string){
  Swal.fire({
    icon: 'error',
    text: msg,
  })
  return
}

export function getAdminDataThunk(){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/getAdminData`, {
      headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
    });
    const result = await res.json()
    let adminData = []
    for (let i = 0; i < result.length; i++){
      let object = {
        adminID: result[i].id,
        adminName: result[i].admin_name,
        email: result[i].email,
        role: result[i].role,
        authority: result[i].role,
      }
      adminData.push(object)
    }
    dispatch(getAdminData(adminData))
  }
}

export function deleteMultiAdminThunk(arrayToServer: number[], array: number[]){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/deleteMultiAdmin`, {
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ adminIDs: arrayToServer })
    });
    const result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    }
    Swal.fire({
      position: 'center',
      icon: 'success',
      title: 'Deletion of Administrator(s) is completed',
      showConfirmButton: false,
      timer: 1500
    })
    setTimeout(()=>{
      dispatch(getAdminDataThunk())
    }, 1500)
  }
}

export function updatePasswordThunk(newPassword: string, oldPassword: string){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/updatePassword`, {
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ newPassword: newPassword, oldPassword: oldPassword })
    });
    const result = await res.text()
    checkResult(result)
  }
}

export function updateThemeThunk(theme: "lightBlue" | "crimson" | "light" | "dark" | "darkMode"){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/updateTheme`, {
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ theme: theme})
    });
    const result = await res.text()
    checkResult(result)
    if (result === "success"){
      setTimeout(()=>{
        dispatch(updateTheme(theme))
      }, 1500)
    }
  }
}

export function addAdminThunk(email: string, adminName: string, password: string, role: "Super" | "Ordinary" | "Intern"){
    
  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/addAdmin`, {
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ email: email, adminName: adminName, password: password, role: role})
    });
    const result = await res.json()
    checkResult(result)
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Admin registration is successful.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getAdminDataThunk())
      }, 1500)
    }
  } 
}

export function updateAdminThunk(data: {adminName: string, email: string, role: string}){
  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/updateAdmin`, {
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ adminName: data.adminName, email: data.email, role: data.role})
    });
    const result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Saved Changes.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(loadTokenThunk())
      }, 1500)
    }
  }
}

export function editMultiAdminThunk(content: {id: number, email: string | null, adminName: string | null, role: "Super" | "Ordinary" | "Intern" | null}[]){

  return async (dispatch: IRootThunkDispatch) => {
    const res = await fetch(`${REACT_APP_API_SERVER}/updateMultiAdmin`, {
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
      },
      body: JSON.stringify({ content: content})
    });
    const result = await res.json()
    if (result.msg){
      failAlert(result.msg)
      return
    } else {
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Saved Changes.',
        showConfirmButton: false,
        timer: 1500
      })
      setTimeout(()=>{
        dispatch(getAdminDataThunk())
      }, 1500)
    }
  }
}

