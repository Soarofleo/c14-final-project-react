import { Columns, Tables } from "./state"

export function loading(){
  return {
    type: "@@Console/loading" as const,
  }
}

export function loadTables(tables: Tables){
  return {
    type: "@@Console/loadTables" as const,
    tables
  }
}

export function loadColumns(columns: Columns){
  return {
    type: "@@Console/loadColumns" as const,
    columns
  }
}

export function loadTableContent(content: string[]){
  return {
    type: "@@Console/loadTableContent" as const,
    content
  }
}

export function resetTableContent(){
  return {
    type: "@@Console/resetTableContent" as const,
  }
}

export function deleteRowSuccess(){
  return {
    type: "@@Console/deleteRowSuccess" as const,
  }
}

export function deleteRowFail(){
  return {
    type: "@@Console/deleteRowFail" as const,
  }
}

export function failed(msg: string){
  return {
    type: "@@Console/failed" as const,
    msg,
  }
}

export type IConsoleAction =
  | ReturnType<typeof loading>
  | ReturnType<typeof loadTables>
  | ReturnType<typeof loadColumns>
  | ReturnType<typeof loadTableContent>
  | ReturnType<typeof resetTableContent>
  | ReturnType<typeof deleteRowSuccess>
  | ReturnType<typeof deleteRowFail>
  | ReturnType<typeof failed>