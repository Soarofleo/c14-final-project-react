import { IConsoleAction } from "./action";
import { IConsoleState, initialState } from "./state";


export const ConsoleReducer = (
  state: IConsoleState = initialState,
  action: IConsoleAction,
): IConsoleState => {
  switch (action.type) {
    case '@@Console/loading':
      return {
        ...state,
        status: 'loading'
      };
    case '@@Console/failed':
      return {
        ...state,
        status: 'error',
        msg: action.msg
      };
    case '@@Console/loadTables':
      return {
        ...state,
        status: 'ready',
        tables: action.tables
      }
    case '@@Console/loadColumns':
      return {
        ...state,
        columns: action.columns
      }
    case '@@Console/loadTableContent':
      return {
        ...state,
        content: action.content
      }
    case '@@Console/resetTableContent':
      return {
        ...state,
        content: []
      }
    case '@@Console/deleteRowSuccess':
      return {
        ...state,
        deleteRowResult: true
      }
    case '@@Console/deleteRowFail':
      return {
        ...state,
        deleteRowResult: false
      }
    default:
      return state;
  }
}