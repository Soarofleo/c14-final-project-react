export type Tables = {table_name: string}[]
export type Columns = {column_name: string}[]

export type IConsoleState = 
| {
    status: "loading",
    msg?: string,
    tables?: Tables,
    columns?: Columns,
    content?: any[],
    deleteRowResult?: boolean,
  }
| {
    status: "ready",
    msg?: string,
    tables: Tables,
    columns?: Columns,
    content?: any[],
    deleteRowResult?: boolean,
  }
| {
    status: "error",
    msg: string,
    tables?: Tables,
    columns?: Columns,
    content?: any[],
    deleteRowResult?: boolean,
  }

export const initialState: IConsoleState = {
  status: "loading"
}