import { IRootThunkDispatch } from '../store';
import { deleteRowFail, deleteRowSuccess, failed, loadColumns, loading, loadTableContent, loadTables } from './action';

const { REACT_APP_API_SERVER } = process.env

export function loadTablesThunk(){
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(loading());
    try{
      const res = await fetch(`${REACT_APP_API_SERVER}/getTables`, {
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
        },
      });
      const result = await res.json()
      if (result.isSuccess === true){
        dispatch(loadTables(result.data))
      } else {
        dispatch(failed('fail to load table'))
      }
    } catch (e){
      dispatch(failed('fail to load table' + e.msg + e.message))
    }
   
  }
}

export function loadColumnsThunk(table: string){
  return async (dispatch: IRootThunkDispatch) => {
    try{
      const res = await fetch(`${REACT_APP_API_SERVER}/getColumns`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
        },
        body: JSON.stringify({ table: table})
      });
      const result = await res.json()
      if (result.isSuccess === true){
        dispatch(loadColumns(result.data))
      } else {
        dispatch(failed('fail to load columns'))
      }
    } catch (e){
      dispatch(failed('fail to load columns' + e.msg + e.message))
    }
  }
}

export function loadTableContentThunk(table: string) {
  return async (dispatch: IRootThunkDispatch) => {
    try{
      const res = await fetch(`${REACT_APP_API_SERVER}/getTableContent`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
        },
        body: JSON.stringify({ table: table})
      });
      const result = await res.json()
      if (result.isSuccess === true){
        dispatch(loadTableContent(result.data))
      } else {
        dispatch(failed('fail to load columns'))
      }
    } catch (e){
      dispatch(failed('fail to load columns' + e.msg + e.message))
    }
  }
}

export function deleteTableRowThunk(table: string, rowID: number){
  return async (dispatch: IRootThunkDispatch) => {
    dispatch(deleteRowFail())
    try{
      const res = await fetch(`${REACT_APP_API_SERVER}/deleteTableRow`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": ("Bearer " + localStorage.getItem('token'))
        },
        body: JSON.stringify({ table: table, rowID: rowID})
      });
      const result = await res.json()
      if (result.isSuccess === true){
        dispatch(deleteRowSuccess())
        dispatch(loadTableContentThunk(table))
      } else {
        dispatch(failed('fail to load columns'))
      }
    } catch (e){
      dispatch(failed('fail to load columns' + e.msg + e.message))
    }
  }
}