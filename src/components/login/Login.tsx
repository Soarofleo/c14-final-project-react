import { useState } from "react";
import { Box, Button, Container, createStyles, makeStyles, TextField } from '@material-ui/core';
import './Login.css'
import { loginThunk } from "../../redux/login/thunk";
import { push } from "connected-react-router";
import { useDispatch } from "react-redux";

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
      justifyContent: 'center',
      width: "100%",
      position: "relative"
    },
    box1: {
      position: "absolute",
      top: 0,
      backgroundColor: "#3f50b5",
      width: "100%",
      display: 'flex',
      justifyContent: 'center',
    },
    container: {
      position: "relative",
    },
    box11: {
      position: "relative",
      top: 0,
      width: "100%",
      height: "10vh",
    },
    logo: {
      width: "8vmax",
      height: "8vmax",
    },
    logoCenter: {
      width: "30vmax",
      height: "30vmax",
    },
    box2: {
      pb: 1,
      pt: 3,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    },
    box3: {
      py: 2
    }
  }),
);

export default function Login(){
  const classes = useStyles();
  const dispatch = useDispatch();
  const [emailStatus, setEmailStatus] = useState(false)
  const [passwordStatus, setPasswordStatus] = useState(false)


  function handleSubmit(event: any){
    event.preventDefault()
    if (event.target.email.value === ""){
      setEmailStatus(true)
    }
    if (event.target.password.value === ""){
      setPasswordStatus(true)
    }
    dispatch(push("/"))
    dispatch(loginThunk(event.target.email.value, event.target.password.value))
  }

  function handleChange(){
    if (emailStatus === true){
      setEmailStatus(false)
    }
    if (passwordStatus === true){
      setPasswordStatus(false)
    }
  }

  return (
    <>
      <Box className={classes.root}>
        <Box className={classes.box1}>
          <img className={classes.logo} src="/Logo2.svg" alt="Logo2"/>
        </Box>
        <Container maxWidth="sm" className={classes.container}>
          <form onSubmit={handleSubmit}>
            <Box className={classes.box11}>
            </Box>
            <Box className={classes.box2}>
              <img className={classes.logoCenter} src="/Logo.svg" alt="Logo"/>
            </Box>
            <TextField
              error={emailStatus}
              fullWidth
              helperText={emailStatus === true ? "Email is required!" : ""}
              label="Email Address"
              margin="normal"
              name="email"
              onChange={handleChange}
              type="email"
              variant="outlined"
            />
            <TextField
              error={passwordStatus}
              fullWidth
              helperText={passwordStatus === true ? "Password is required!" : ""}
              label="Password"
              margin="normal"
              name="password"
              onChange={handleChange}
              type="password"
              variant="outlined"
            />
            <Box className={classes.box3} style={{marginTop: "16px"}}>
              <Button
                color="primary"
                fullWidth
                size="large"
                type="submit"
                variant="contained"
              >
                Login
              </Button>
            </Box>
          </form>
        </Container>
      </Box>
    </>
  );
}