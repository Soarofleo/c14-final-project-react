import { createStyles, makeStyles, Theme } from "@material-ui/core";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getUserDataThunk } from "../../redux/user/thunk";
import UserDetail from "./UserDetail";
import UserList from "./UserList";
import UserSearch from "./UserSearch";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);


export default function Users(){
  const classes = useStyles();
  const dispatch = useDispatch();
  useEffect(()=>{
    dispatch(getUserDataThunk())
  }, [dispatch])

  return(
    <main className={classes.content}>
      <div className={classes.toolbar} />
      {window.location.href.slice(window.location.href.search("Users/")).slice(6) === ""
        ?<>
          <UserSearch />
          <UserList />
        </>
        : <UserDetail />}
    </main>
  )
}