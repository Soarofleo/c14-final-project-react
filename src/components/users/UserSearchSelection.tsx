import { createStyles, FormControl, makeStyles, Select, Theme } from '@material-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { changeUserSearchType } from '../../redux/user/action';


const useStyles = makeStyles((theme: Theme) =>
createStyles({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}),
);

export default function UserSearchSelection (){

  const classes = useStyles();
  const dispatch = useDispatch();

  const handleChange = (event: React.ChangeEvent<{ name?: string; value: any }>) => {
    let num = parseInt(event.target.value)
    if (num === 0){
      dispatch(changeUserSearchType("userID"))
    } else if (num === 1){
      dispatch(changeUserSearchType("username"))
    } else {
      dispatch(changeUserSearchType("email"))
    }
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <Select
        native
        onChange={handleChange}
        placeholder="Bar Chart"
      >
        <option value={0}>User ID</option>
        <option value={1}>Username</option>
        <option value={2}>Email</option>
      </Select>
    </FormControl>
  )
}