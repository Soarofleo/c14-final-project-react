import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import CircularProgress from '@material-ui/core/CircularProgress';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Switch, Avatar, Box, Button, Card, CardActions, CardContent, createStyles, Dialog, Divider, Fade, FormControl, FormControlLabel, GridList, GridListTile, GridListTileBar, IconButton, Input, makeStyles, Modal, Paper, Select, Table, TableBody, TableCell, TableContainer, TableRow, TextField, Toolbar, Tooltip, Typography, TableHead, DialogTitle, Backdrop, DialogContent, DialogActions, InputAdornment } from "@material-ui/core";
import { Link, useRouteMatch } from "react-router-dom";
import { blockUserThunk, deleteMultiUserThunk, deletePhotoThunk, editMultiUsernameThunk, editUserDetailThunk, getUserDetailThunk, removeProfilePicThunk, unblockUserThunk, updateMultiCoinThunk } from "../../redux/user/thunk";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import BlockIcon from '@material-ui/icons/Block';
import UndoIcon from '@material-ui/icons/Undo';
import { push } from "connected-react-router";
import { Alert } from "@material-ui/lab";
import EditIcon from '@material-ui/icons/Edit';
import CancelIcon from '@material-ui/icons/Cancel';
import SaveIcon from '@material-ui/icons/Save';
import { dateToInputType } from "../utility/date";
import MaskedInput from 'react-text-mask';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import MoneyOffIcon from '@material-ui/icons/MoneyOff';

const { REACT_APP_CDN_URL } = process.env

const useStyles = makeStyles(()=>
  createStyles({
    root:{
      display: "flex",
      justifyContent: "space-evenly",
      flexWrap: "wrap",
      '& > *': {
        margin: 20,
      },
      height: "100%",
    },
    card:{
      maxWidth: 400,
    },
    box:{
      alignItems: 'center',
      display: 'flex',
      flexDirection: 'column',
      position: 'relative',
    },
    box2:{
      alignItems: 'center',
      justifyContent: "space-evenly",
      display: 'flex',
      flexDirection: 'column',
      height: "100%",
    },
    profile:{
      width: 200,
      height: 200,
      objectFit: "contain",
    },
    table:{
      maxWidth: 500,
      maxHeight: 600,
    },
    table2:{
      maxWidth: 620,
      maxHeight: 600,
    },
    table3:{
      maxWidth: 800,
      maxHeight: 600,
    },
    gridList: {
      width: 400,
      height: 400,
      display: "flex",
      justifyContent: "space-evenly",
    },
    titleBar: {
      background:
        'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
        'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    title: {
      textAlign: "center",
      margin: 10,
    },
    modal: {
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'flex-end',
      margin: 20,
    },
    name: {
      marginTop: 10,
      position: 'relative',
      // marginBottom: 5,
    },
    iconBtn: {
      position: "absolute",
      right: 0,
      top: -6,
    },
    iconBtns: {
      position: "relative",
      right: 0,
      top: -6,
    },
    toolbar: {
      display: "flex",
      justifyContent: "space-between",
    },
    icon: {
      color: 'white',
    },
  }),
);

interface TextMaskCustomProps {
  inputRef: (ref: HTMLInputElement | null) => void;
}

function TextMaskCustom(props: TextMaskCustomProps) {
  const { inputRef, ...other } = props;

  return (
    <MaskedInput
      {...other}
      ref={(ref: any) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[/[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]}
      placeholderChar={'\u2000'}
      showMask
    />
  );
}

export default function UserDetail(){
  const classes = useStyles();
  const id = useRouteMatch<{id: string}>().params.id
  const user = useSelector((state: IRootState) => state.user.userData[parseInt(id)]);
  const [dialogOpen, setDialogOpen] = useState(false);
  const [dialog1Open, setDialog1Open] = useState(false);
  const [action, setAction] = useState<"unblock" | "block" | "delete" | "edit" | "deletePic" | null>(null)
  const [editContent, setEditContent] = useState<{
    phone_num: string | null, 
    registration_status: number | null, 
    birthday: string | null, 
    gender: string | null,
    text_description: string | null,
    voice_description: string | null
  }>({
    phone_num: null, 
    registration_status: null, 
    birthday: null, 
    gender: null,
    text_description: null,
    voice_description: null
  })
  const dispatch = useDispatch();
  const [collapse1Open, setCollapse1Open] = useState(false)
  const [collapse2Open, setCollapse2Open] = useState(false)
  const [collapse3Open, setCollapse3Open] = useState(false)
  const [collapse4Open, setCollapse4Open] = useState(false)
  const [collapse5Open, setCollapse5Open] = useState(false)
  const [collapse6Open, setCollapse6Open] = useState(false)
  const [collapse7Open, setCollapse7Open] = useState(false)
  const [loading, setLoading] = useState(true)
  const [multiAction, setMultiAction] = useState<string | null>(null);
  const [alertOpen, setAlertOpen] = useState(false);

  useEffect(()=>{
    dispatch(getUserDetailThunk(parseInt(id)))
    setLoading(true)
    setTimeout(()=>{
      setLoading(false)
    }, 500)
  }, [dispatch, id])

  useEffect(()=>{
    setEditContent({
      phone_num: null, 
      registration_status: null, 
      birthday: null, 
      gender: null,
      text_description: null,
      voice_description: null
    }) 
  }, [action])

  const userDetail = useSelector((state: IRootState) => state.user.userDetail);  
  const currentAdmin = useSelector((state: IRootState) => state.login.isLogin === false ? null :state.login.currentAdmin);
  if (!currentAdmin){return (<CircularProgress />)}

  let profile: any;
  for (let i = 0; i < userDetail.user_photos.length; i++){
    if (userDetail.user_photos[i].description === "profile"){
      profile = userDetail.user_photos[i].url   
    }
  }

  const handleDialogClickOpen = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setAction(null)
    setDialogOpen(false);
  };

  function deleteUser(){
    if (action === "delete"){
      dispatch(deleteMultiUserThunk([userDetail.id]))
      dispatch(push('/Users'))
    } else if (action === "block"){
      dispatch(blockUserThunk(userDetail.id))
    } else if (action === "unblock"){
      dispatch(unblockUserThunk(userDetail.id))
    }
    setDialogOpen(false);
  }

  function clickRemove(){
    dispatch(removeProfilePicThunk(userDetail.id, profile))
  }

  function saveEditContent(event: any){
    let object = {...editContent}
    if (event.target.name === "phone_num"){
      if (event.target.value === userDetail.phone_num){
        object.phone_num = null
      } else {
        object.phone_num = event.target.value
      }
    }
    if (event.target.name === "registration_status"){
      if (event.target.value === userDetail.registration_status){
        object.registration_status = null
      } else {
        object.registration_status = event.target.value
      }
    }
    if (event.target.name === "birthday"){
      if (event.target.value === userDetail.birthday){
        object.birthday = null
      } else {
        object.birthday = event.target.value
      }
    }
    if (event.target.name === "gender"){
      if (event.target.value === userDetail.gender){
        object.gender = null
      } else {
        object.gender = event.target.value
      }
    }
    if (event.target.name === "text_description"){
      if (event.target.value === userDetail.text_description){
        object.text_description = null
      } else {
        object.text_description = event.target.value
      }
    }
    if (event._reactName === "onClick"){
      object.voice_description = event._reactName
    }
    setEditContent(object)
  }

  function deletePhoto(url: string){
    dispatch(deletePhotoThunk(url, userDetail.id))
  }

  function redirectToUserPage(id: number){
    dispatch(push(`/Users/${id}`));
    dispatch(getUserDetailThunk(id))
  }

  setTimeout(()=>{
    setLoading(false)
  }, 500)

  const handleDialog1ClickOpen = () => {
    setDialog1Open(true);
  };

  const handleDialog1Close = () => {
    setDialog1Open(false);
  };

  function handleAlertClose () {
    setAlertOpen(false);
  }

  function editMultiUsername() {
    let inputValue = (document.querySelector('#newUsername') as HTMLInputElement).value
    if (inputValue === "") { return }
    if (multiAction === "username"){
      dispatch(editMultiUsernameThunk([userDetail.id], [], inputValue, "userDetail"))
      setDialog1Open(false);
    } else if (multiAction === "giveCoin"){
      let inputDescription = (document.querySelector('#coinDescription') as HTMLInputElement).value
      let newCoin = parseInt(inputValue)
      dispatch(updateMultiCoinThunk([userDetail.id], [], newCoin, inputDescription, "userDetail"))
      setDialog1Open(false);
    } else if (multiAction === "drawCoin"){
      let inputDescription = (document.querySelector('#coinDescription') as HTMLInputElement).value
      if (userDetail.coins && userDetail.coins < parseInt(inputValue)){
        setAlertOpen(true);
        return
      }
      let newCoin = parseInt(inputValue)*(-1)
      dispatch(updateMultiCoinThunk([userDetail.id], [], newCoin, inputDescription, "userDetail"))
      setDialog1Open(false);
    }
    return
  }

  if (loading || userDetail.id === 0){
    return (
      <CircularProgress />
    )
  }

  return (
    <div>
      <IconButton title="Return" component={Link} to="/Users"><ArrowBackIcon /></IconButton>
      { currentAdmin.role === "Intern"
        ? ""
        :
        <>
        <IconButton title="Give coins" aria-label="give coin" onClick={() => { setMultiAction("giveCoin"); handleDialog1ClickOpen() }}>
          <MonetizationOnIcon />
        </IconButton>
        <IconButton title="Deduct coins" aria-label="draw coin" onClick={() => { setMultiAction("drawCoin"); handleDialog1ClickOpen() }}>
          <MoneyOffIcon />
        </IconButton>
        {userDetail.isBlocked === true
          ?<IconButton title="Unblock" onClick={() => { setAction("unblock"); handleDialogClickOpen() }}><UndoIcon /></IconButton>
          :<IconButton title="Block" onClick={() => { setAction("block"); handleDialogClickOpen() }}><BlockIcon /></IconButton>}
        <IconButton title="Delete" onClick={() => { setAction("delete"); handleDialogClickOpen() }}><DeleteForeverIcon /></IconButton>
        </>
      }
      <div className={classes.root}>
      {userDetail.id === 0 
      ?<CircularProgress /> 
      :<>
        <Card className={classes.card}>
          <CardContent>
            <Box className={classes.box}>
              <Avatar src={REACT_APP_CDN_URL as string + profile} className={classes.profile}/>
              <Tooltip title="Edit username" style={{position: 'absolute', right: -20, top: 175}}>
                  <IconButton aria-label="rename" onClick={() => { setMultiAction("username"); handleDialog1ClickOpen() }}>
                    <EditIcon />
                  </IconButton>
                </Tooltip>
              <Typography
                color="textPrimary"
                gutterBottom
                variant="h4"
                className={classes.name}
              >
                {userDetail.username}
                
              </Typography>
              <Typography
                color="textSecondary"
                variant="body1"
              >
                User ID: {userDetail.id}
              </Typography>
              
              <Typography
                color="textSecondary"
                variant="body1"
              >
                Email: {userDetail.email}
              </Typography>
              <Typography
                color="textSecondary"
                variant="body1"
                style={{position: 'relative'}}
              >
                Coins: {userDetail.coins}
                
              </Typography>
              <Typography
                color="textSecondary"
                variant="body1"
              >
                Last Online: {(new Date(userDetail.created_at as string)).toDateString().slice(11) + " " + (new Date(userDetail.created_at as string)).toDateString().slice(4, 10) + " " + (new Date(userDetail.created_at as string)).toTimeString().slice(0, 8)}
              </Typography>
            </Box>
          </CardContent>
          {currentAdmin.role !== "Intern"
          ?<>
            <CardActions>
              <Button
                color="primary"
                fullWidth
                variant="contained"
                onClick={clickRemove}
              >
                Remove picture
              </Button>
            </CardActions>
          </>
          :""}
        </Card>
        <TableContainer component={Paper} className={classes.table}>
          {currentAdmin.role !== "Intern"
          ?<Toolbar className={classes.toolbar}>
            <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
              
            </Typography>
            <div>
            {
              action !== "edit"
              ? 
              <Tooltip title="Edit">
                <IconButton onClick={() => { setAction("edit") }}>
                  <EditIcon />
                </IconButton>
              </Tooltip>
              : <>
              <Tooltip title="Save">
                <IconButton onClick={() => { dispatch(editUserDetailThunk(editContent, userDetail.id)); setAction(null) }}>
                  <SaveIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Cancel">
                <IconButton onClick={() => { 
                  setAction(null); 
                  setEditContent({
                    phone_num: null, 
                    registration_status: null, 
                    birthday: null, 
                    gender: null,
                    text_description: null,
                    voice_description: null
                  }) 
                }}>
                  <CancelIcon />
                </IconButton>
              </Tooltip>
              </>
            }
            </div>
          </Toolbar>
          :""
          }
          <Divider/>
          <Table aria-label="simple table" >
            <TableBody>
              <TableRow>
                <TableCell component="th" scope="row">
                  Phone Number:
                </TableCell>
                <TableCell align="right">
                {
                  action === "edit"
                  ?<FormControl>
                    <Input
                      defaultValue={userDetail.phone_num} 
                      onChange={saveEditContent}
                      name="phone_num"
                      id="formatted-text-mask-input"
                      inputComponent={TextMaskCustom as any}
                    />
                  </FormControl>
                  :userDetail.phone_num
                }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Registration status: 
                </TableCell>
                <TableCell align="right">
                {
                  action === "edit"
                  ?<Select
                    native
                    defaultValue={userDetail.registration_status}
                    onChange={saveEditContent}
                    inputProps={{
                      name: 'registration_status',
                      id: 'registration_status',
                    }}
                  >
                    <option value={0}>0</option>
                    <option value={1}>1</option>
                    <option value={2}>2</option>
                    <option value={3}>3</option>
                  </Select>
                  :userDetail.registration_status
                }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Birthday:  
                </TableCell>
                <TableCell align="right">
                {
                  action === "edit"
                  ?<input type="date" name="birthday" defaultValue={dateToInputType(userDetail.birthday)} onChange={saveEditContent}></input>
                  :userDetail.birthday
                }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Gender:  
                </TableCell>
                <TableCell align="right">
                {
                  action === "edit"
                  ?<Select
                  native
                  defaultValue={userDetail.registration_status}
                  onChange={saveEditContent}
                  inputProps={{
                    name: 'gender',
                    id: 'gender',
                  }}
                >
                  <option value={'male'}>Female</option>
                  <option value={'female'}>Male</option>
                  <option value={'其他'}>Other</option>
                </Select>
                  :<>{userDetail.gender === 'male' ? "Female" : <>{userDetail.gender === 'female' ? "Male" : "Other"}</>}</>
                }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Registration Date:  
                </TableCell>
                <TableCell align="right">
                {
                  userDetail.created_at
                }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Text Description:  
                </TableCell>
                <TableCell align="right">
                {
                  action === "edit"
                  ?<TextField 
                  id={`text_description-${user?.userID}`}
                  name="text_description"
                  variant="filled" 
                  defaultValue={userDetail.text_description} 
                  onChange={saveEditContent}/> 
                  :userDetail.text_description
                }
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Block Status:  
                </TableCell>
                <TableCell align="right">{userDetail.isBlocked === false ? "Normal" : "Blocked"}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  AI Accuracy:  
                </TableCell>
                <TableCell align="right">{userDetail.model_accuracy}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell component="th" scope="row">
                  Robot:  
                </TableCell>
                <TableCell align="right">{userDetail.isFake === false ? "No" : "Yes"}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
          <div style={{backgroundColor: currentAdmin.theme === "crimson"? "crimson": currentAdmin.theme === "light" ? "#dfdfdf": currentAdmin.theme === "dark" ? "#2e2e2e": "#3f51b5", height: 200, width: 200, borderRadius: "50%", display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
            <div style={{backgroundColor: 'white', height: 180, width: 180, borderRadius: "50%"}}>
              <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', height: 180, width: 180, }}>
                <div style={{fontSize: 18}}>AI Accuracy:</div>
                <div style={{fontSize: 50}}>{Math.round(userDetail.model_accuracy * 10000)/100 + '%'}</div>
              </div>
            </div>
          </div>
        <Card className={classes.card}>
          <Toolbar className={classes.toolbar}>
            <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
              Photos
            </Typography>
            <div>
              { currentAdmin.role !== "Intern"
              ?<FormControlLabel
                control={<Switch checked={action === "deletePic"} onChange={() => {action !== "deletePic" ? setAction("deletePic") : setAction(null) }} name="deletePic" />}
                label="Edition Mode"
              />
              :""}
            </div>
          </Toolbar>
          <GridList className={classes.gridList} cols={3}>
            {userDetail.user_photos.map((photo) => (
              photo.description === "profile"
              ? ""
              :
              <GridListTile key={photo.id} cols={1} >
                <img src={REACT_APP_CDN_URL as string + photo.url} alt={photo.description as string} />
                <GridListTileBar 
                  // title={photo.description} 
                  titlePosition="top" 
                  className={classes.titleBar}
                  actionIcon={
                    action === "deletePic"
                    ?<IconButton className={classes.icon} onClick={()=>{deletePhoto(photo.url as string)}}>
                      <DeleteForeverIcon />
                    </IconButton>
                    :""
                  }
                />
              </GridListTile>
            ))}
          </GridList>
        </Card>
        <TableContainer component={Paper} className={classes.table2}>
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            User's Tags
            <IconButton aria-label="expand row" size="small" onClick={() => collapse5Open ?setCollapse5Open(false) :setCollapse5Open(true)}>
              {collapse3Open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </Typography>
          <Divider />
          {collapse5Open
          ?<Table aria-label="simple table" >
            <TableHead>
              <TableRow>
                <TableCell align="right">Tag</TableCell>
                <TableCell align="right">Class</TableCell>
                <TableCell align="right">Date</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {userDetail.userTags.map((item, index) => (
              <TableRow key={'userTags' + index}>
                <TableCell align="right">{item.tag}</TableCell>
                <TableCell align="right">{item.class === "習慣1" ? '吸煙習慣' : item.class === "習慣2" ? '飲酒習慣' : item.class}</TableCell>
                <TableCell align="right">{(new Date(item.created_at as string)).toDateString().slice(11) + " " + (new Date(item.created_at as string)).toDateString().slice(4, 10) + " " + (new Date(item.created_at as string)).toTimeString().slice(0, 8)}</TableCell>
              </TableRow>
            ))}
            </TableBody>
          </Table>
          :null
          }
        </TableContainer>
        <TableContainer component={Paper} className={classes.table2}>
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            User's Wish Tags
            <IconButton aria-label="expand row" size="small" onClick={() => collapse6Open ?setCollapse6Open(false) :setCollapse6Open(true)}>
              {collapse3Open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </Typography>
          <Divider />
          {collapse6Open
          ?<Table aria-label="simple table" >
            <TableHead>
              <TableRow>
                <TableCell align="right">Wish Tag</TableCell>
                <TableCell align="right">Class</TableCell>
                <TableCell align="right">Date</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {userDetail.userWishTags.map((item, index) => (
              <TableRow key={'userWishTags' + index}>
                <TableCell align="right">{item.tag}</TableCell>
                <TableCell align="right">{item.class === "習慣1" ? '吸煙習慣' : item.class === "習慣2" ? '飲酒習慣' : item.class}</TableCell>
                <TableCell align="right">{(new Date(item.created_at as string)).toDateString().slice(11) + " " + (new Date(item.created_at as string)).toDateString().slice(4, 10) + " " + (new Date(item.created_at as string)).toTimeString().slice(0, 8)}</TableCell>
              </TableRow>
            ))}
            </TableBody>
          </Table>
          :null
          }
        </TableContainer>
        <TableContainer component={Paper} className={classes.table2}>
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            Transactions
            <IconButton aria-label="expand row" size="small" onClick={() => collapse4Open ?setCollapse4Open(false) :setCollapse4Open(true)}>
              {collapse3Open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </Typography>
          <Divider />
          {collapse4Open
          ?<Table aria-label="simple table" >
            <TableHead>
              <TableRow>
                <TableCell align="right">ID</TableCell>
                <TableCell align="right">Amount</TableCell>
                <TableCell align="right">Description</TableCell>
                <TableCell align="right">Created by</TableCell>
                <TableCell align="right">Date</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {userDetail.user_transactions.map((transaction) => (
              <TableRow key={transaction.id}>
                <TableCell component="th" scope="row">{transaction.id}</TableCell>
                <TableCell align="right">{transaction.transaction}</TableCell>
                <TableCell align="right">{transaction.description}</TableCell>
                <TableCell align="right">{transaction.created_by}</TableCell>
                <TableCell align="right">{(new Date(transaction.created_at as string)).toDateString().slice(11) + " " + (new Date(transaction.created_at as string)).toDateString().slice(4, 10) + " " + (new Date(transaction.created_at as string)).toTimeString().slice(0, 8)}</TableCell>
              </TableRow>
            ))}
            </TableBody>
          </Table>
          :null
          }
        </TableContainer>
        <TableContainer component={Paper} className={classes.table2}>
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
          {userDetail.userLike.length} person(s) be Liked by {userDetail.username} 
            <IconButton aria-label="expand row" size="small" onClick={() => collapse1Open ?setCollapse1Open(false) :setCollapse1Open(true)}>
              {collapse1Open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </Typography>
          <Divider />
          {collapse1Open
          ?<Table aria-label="simple table" >
            <TableHead>
              <TableRow>
                <TableCell align="right">User id</TableCell>
                <TableCell align="right">Username</TableCell>
                <TableCell align="right">Email</TableCell>
                <TableCell align="right">Location</TableCell>
                <TableCell align="right">Scheduled</TableCell>
                <TableCell align="right">Time</TableCell>
                <TableCell align="right"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {userDetail.userLike.map((item) => (
              <TableRow key={'userLike' + item.friend_id}>
                <TableCell component="th" scope="row">{item.friend_id}</TableCell>
                <TableCell align="right">{item.username}</TableCell>
                <TableCell align="right">{item.email}</TableCell>
                <TableCell align="right">({item.match_coordinates.x}, {item.match_coordinates.y})</TableCell>
                <TableCell align="right">{item.isScheduled ?"yes" :"no"}</TableCell>
                <TableCell align="right">{(new Date(item.created_at as string)).toDateString().slice(11) + " " + (new Date(item.created_at as string)).toDateString().slice(4, 10) + " " + (new Date(item.created_at as string)).toTimeString().slice(0, 8)}</TableCell>
                <TableCell>
                  <IconButton aria-label="expand row" size="small" onClick={()=>redirectToUserPage(item.friend_id as number)}>
                    <KeyboardArrowRightIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
            </TableBody>
          </Table>
          :null
          }
        </TableContainer>
        <TableContainer component={Paper} className={classes.table2}>
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            {userDetail.userBeLiked.length} person(s) Like {userDetail.username} 
            <IconButton aria-label="expand row" size="small" onClick={() => collapse2Open ?setCollapse2Open(false) :setCollapse2Open(true)}>
              {collapse2Open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </Typography>
          <Divider />
          {collapse2Open
          ?<Table aria-label="simple table" >
            <TableHead>
              <TableRow>
                <TableCell align="right">User id</TableCell>
                <TableCell align="right">Username</TableCell>
                <TableCell align="right">Email</TableCell>
                <TableCell align="right">Location</TableCell>
                <TableCell align="right">Scheduled</TableCell>
                <TableCell align="right">Time</TableCell>
                <TableCell align="right"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {userDetail.userBeLiked.map((item) => (
              <TableRow key={'userBeLiked' + item.user_id}>
                <TableCell component="th" scope="row">{item.user_id}</TableCell>
                <TableCell align="right">{item.username}</TableCell>
                <TableCell align="right">{item.email}</TableCell>
                <TableCell align="right">({item.match_coordinates.x}, {item.match_coordinates.y})</TableCell>
                <TableCell align="right">{item.isScheduled ?"yes" :"no"}</TableCell>
                <TableCell align="right">{(new Date(item.created_at as string)).toDateString().slice(11) + " " + (new Date(item.created_at as string)).toDateString().slice(4, 10) + " " + (new Date(item.created_at as string)).toTimeString().slice(0, 8)}</TableCell>
                <TableCell>
                  <IconButton aria-label="expand row" size="small" onClick={()=>redirectToUserPage(item.user_id as number)}>
                    <KeyboardArrowRightIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
            </TableBody>
          </Table>
          :null
          }
        </TableContainer>
        <TableContainer component={Paper} className={classes.table3}>
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            {userDetail.matched.length} match(es) 
            <IconButton aria-label="expand row" size="small" onClick={() => collapse3Open ?setCollapse3Open(false) :setCollapse3Open(true)}>
              {collapse3Open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </Typography>
          <Divider />
          {collapse3Open
          ?<Table aria-label="simple table" >
            <TableHead>
              <TableRow>
                <TableCell align="right">Friend id</TableCell>
                <TableCell align="right">Name</TableCell>
                <TableCell align="right">Email</TableCell>
                <TableCell align="right">User's match location</TableCell>
                <TableCell align="right">Friend's match location</TableCell>
                <TableCell align="right">Scheduled</TableCell>
                <TableCell align="right">User's like time</TableCell>
                <TableCell align="right">Friend's like time</TableCell>
                <TableCell align="right"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {userDetail.matched.map((item) => (
              <TableRow key={'matched' + item.friend_id}>
                <TableCell component="th" scope="row">{item.friend_id}</TableCell>
                <TableCell align="right">{item.username}</TableCell>
                <TableCell align="right">{item.email}</TableCell>
                <TableCell align="right">({item.user_match_coordinates.x}, {item.user_match_coordinates.y})</TableCell>
                <TableCell align="right">({item.friend_match_coordinates.x}, {item.friend_match_coordinates.y})</TableCell>
                <TableCell align="right">{item.isScheduled ?"yes" :"no"}</TableCell>
                <TableCell align="right">{(new Date(item.user_like_time as string)).toDateString().slice(11) + " " + (new Date(item.user_like_time as string)).toDateString().slice(4, 10) + " " + (new Date(item.user_like_time as string)).toTimeString().slice(0, 8)}</TableCell>
                <TableCell align="right">{(new Date(item.friend_like_time as string)).toDateString().slice(11) + " " + (new Date(item.friend_like_time as string)).toDateString().slice(4, 10) + " " + (new Date(item.friend_like_time as string)).toTimeString().slice(0, 8)}</TableCell>
                <TableCell>
                  <IconButton aria-label="expand row" size="small" onClick={()=>redirectToUserPage(item.friend_id as number)}>
                    <KeyboardArrowRightIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
            </TableBody>
          </Table>
          :null
          }
        </TableContainer>
        <TableContainer component={Paper} className={classes.table3}>
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            {userDetail.schedule.length} schedule(s) 
            <IconButton aria-label="expand row" size="small" onClick={() => collapse7Open ?setCollapse7Open(false) :setCollapse7Open(true)}>
              {collapse7Open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </Typography>
          <Divider />
          {collapse7Open
          ?<Table aria-label="simple table" >
            <TableHead>
              <TableRow>
                <TableCell align="right">Schedule id</TableCell>
                <TableCell align="right">Inviter</TableCell>
                <TableCell align="right">Inviter id</TableCell>
                <TableCell align="right">Invitee</TableCell>
                <TableCell align="right">Invitee id</TableCell>
                <TableCell align="right">Acceptance</TableCell>
                <TableCell align="right">Schedule date</TableCell>
                <TableCell align="right">Created at</TableCell>
                <TableCell align="right"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {userDetail.schedule.map((item) => (
              <TableRow key={'schedule' + item.id}>
                <TableCell component="th" scope="row">{item.id}</TableCell>
                <TableCell align="right">{item.inviter}</TableCell>
                <TableCell align="right">{item.inviter_id}</TableCell>
                <TableCell align="right">{item.invitee}</TableCell>
                <TableCell align="right">{item.invitee_id}</TableCell>
                <TableCell align="right">{item.is_accepted ? "yes" : 'no'}</TableCell>
                <TableCell align="right">{(new Date(item.scheduled_at as string)).toDateString().slice(11) + " " + (new Date(item.scheduled_at as string)).toDateString().slice(4, 10) + " " + (new Date(item.scheduled_at as string)).toTimeString().slice(0, 8)}</TableCell>
                <TableCell align="right">{(new Date(item.created_at as string)).toDateString().slice(11) + " " + (new Date(item.created_at as string)).toDateString().slice(4, 10) + " " + (new Date(item.created_at as string)).toTimeString().slice(0, 8)}</TableCell>
                <TableCell>
                  <IconButton aria-label="expand row" size="small" onClick={()=>item.inviter_id === parseInt(id)?redirectToUserPage(item.invitee_id as number) :redirectToUserPage(item.inviter_id as number)}>
                    <KeyboardArrowRightIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
            </TableBody>
          </Table>
          :null
          }
        </TableContainer>
      </>
      }
      </div>
      <Dialog open={dialogOpen} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
        <Alert
          severity="warning"
          action={
            <Button variant="contained" color="secondary" size="small" onClick={deleteUser}>
              Confirm
            </Button>
          }
        >
          {action === "delete" 
            ?`User ${userDetail.username} will be deleted.` 
            : action === "block"
              ?`User ${userDetail.username} will be blocked.`
              :`User ${userDetail.username} will be unblocked.`}
        </Alert>
      </Dialog>
      <Dialog open={dialog1Open} onClose={handleDialog1Close} aria-labelledby="form-dialog-title">
        {
          <>
            <DialogTitle id="form-dialog-title">
              {multiAction === "username" 
                ?"Edit Username" 
                : multiAction === "giveCoin"
                  ?"Give Coins"
                  :"Deduct Coins"
              }
            </DialogTitle>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                id="newUsername"
                label={multiAction === "username" ?"New username" : "Coins"}
                InputProps={multiAction === "username" ?{} :{
                  startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  inputProps: { min: 0, max: 9999 }
                }}
                type={multiAction === "username" ?"" :"number"}
                required
                fullWidth
                defaultValue={userDetail.username}
              />
              {multiAction !== "username"
              ?<TextField
                autoFocus
                margin="dense"
                id="coinDescription"
                label="Coin action description"
                type=""
                required
                fullWidth
              />
              :""
              }
            </DialogContent>
            <DialogActions>
              <Button onClick={editMultiUsername} color="primary">
                Confirm
              </Button>
              <Button onClick={handleDialog1Close} color="primary">
                Cancel
              </Button>
            </DialogActions>
          </>
        }
      </Dialog>
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={alertOpen}
          onClose={handleAlertClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={alertOpen}>
            <Alert severity="warning">
              There are insufficient coins — <strong>check it out!</strong>
            </Alert>
          </Fade>
        </Modal>
    </div>
  )
}