import React from 'react';
import clsx from 'clsx';
import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { push } from 'connected-react-router';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import EditIcon from '@material-ui/icons/Edit';
import MoneyOffIcon from '@material-ui/icons/MoneyOff';
import { Backdrop, Button, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, Fade, InputAdornment, Modal, TextField } from '@material-ui/core';
import { deleteMultiUserThunk, editMultiUsernameThunk, updateMultiCoinThunk } from '../../redux/user/thunk';
import { Alert } from '@material-ui/lab';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

interface Data {
  userID: number;
  username: string;
  email: string;
  coins: string;
  lastOnline: string;
}


function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key,
): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
}

const headCells: HeadCell[] = [
  { id: 'userID', numeric: true, disablePadding: false, label: 'User ID' },
  { id: 'username', numeric: false, disablePadding: true, label: 'Username' },
  { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
  { id: 'coins', numeric: true, disablePadding: false, label: 'Coins' },
  { id: 'lastOnline', numeric: false, disablePadding: false, label: 'Last online' },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  numSelected: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}



const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
        : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
    title: {
      flex: '1 1 100%',
    },
  }),
);

interface EnhancedTableToolbarProps {
  numSelected: number;
}



const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    darkModePaper: {
      width: '100%',
      marginBottom: theme.spacing(2),
      backgroundColor: "grey",
      color: "white",
    },
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
    modal: {
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'flex-end',
      margin: 20,
    },
    darkMode: {
      backgroundColor: "grey",
      color: "white",
    }
  }),
);

export default function UserList() {
  const classes = useStyles();
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof Data>('userID');
  const [selected, setSelected] = React.useState<number[]>([]);
  const [indexes, setIndexes] = React.useState<number[]>([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [dialogOpen, setDialogOpen] = React.useState(false);
  const [multiAction, setMultiAction] = React.useState<string | null>(null);
  const [alertOpen, setAlertOpen] = React.useState(false);
  const dispatch = useDispatch();
  const keyword = useSelector((state: IRootState) => state.user.userSearchKeyword);
  const rowsWithoutSearch = useSelector((state: IRootState) => state.user.userData);
  const rowsAfterSearch = useSelector((state: IRootState) => state.user.userDataAfterSearch);
  const currentAdmin = useSelector((state: IRootState) => state.login.isLogin === false ? null :state.login.currentAdmin);
  
  if (!currentAdmin){return (<CircularProgress />)}
  let rows = rowsWithoutSearch.slice();
  if (keyword !== "") {
    rows = rowsAfterSearch.slice()
  }

  rows = rowsWithoutSearch.slice();
  if (keyword !== "") {
    rows = rowsAfterSearch.slice()
  }

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof Data) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (currentAdmin.role === "Intern"){
      return
    }
    const newSelecteds = []
    const newIndexes = []
    if (selected.length === 0) {
      for (let i = 0; i < rows.length; i++){
        newSelecteds.push(rows[i].userID)
        newIndexes.push(i)
      }
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
    setIndexes([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, userID: number, index: number) => {
    const selectedIndex = selected.indexOf(userID);
    let newSelected: number[] = [];
    let newIndexes: number[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, userID);
      newIndexes = newIndexes.concat(indexes, index);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
      newIndexes = newIndexes.concat(indexes.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
      newIndexes = newIndexes.concat(indexes.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
      newIndexes = newIndexes.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
    setIndexes(newIndexes);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const isSelected = (userID: number) => selected.indexOf(userID) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  function openUserInfo(id: number) {
    dispatch(push(`/Users/${id}`));
  }

  const handleDialogClickOpen = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  function editMultiUsername() {
    let inputValue = (document.querySelector('#newUsername') as HTMLInputElement).value
    if (inputValue === "") { return }
    if (multiAction === "username"){
      dispatch(editMultiUsernameThunk(selected, indexes, inputValue))
      setDialogOpen(false);
      setSelected([]);
    } else if (multiAction === "giveCoin"){
      let inputDescription = (document.querySelector('#coinDescription') as HTMLInputElement).value
      let newCoin = parseInt(inputValue)
      dispatch(updateMultiCoinThunk(selected, indexes, newCoin, inputDescription))
      setDialogOpen(false);
      setSelected([]);
    } else if (multiAction === "drawCoin"){
      let inputDescription = (document.querySelector('#coinDescription') as HTMLInputElement).value
      for (let i = 0; i < selected.length; i++){
        for (let j = 0; j < rows.length; j++){
          if (rows[j].userID === selected[i] && rows[j].coins < parseInt(inputValue)){
            setAlertOpen(true);
            return
          }
        }
      }
      let newCoin = parseInt(inputValue)*(-1)
      dispatch(updateMultiCoinThunk(selected, indexes, newCoin, inputDescription))
      setDialogOpen(false);
      setSelected([]);
    }
    return
  }

  function handleAlertClose () {
    setAlertOpen(false);
  }

  function deleteMultiUser(){
    setSelected([]);
    setIndexes([]);
    setDialogOpen(false);
    dispatch(deleteMultiUserThunk(selected))
  }

  const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
    const classes = useToolbarStyles();
    const { numSelected } = props;

    return (
      <Toolbar
        className={clsx(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        {numSelected > 0 ? (
          <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            List of Users
          </Typography>
        )}
        {numSelected > 0 ? (
          <>
            <Tooltip title="Edit username">
              <IconButton aria-label="rename" onClick={() => { setMultiAction("username"); handleDialogClickOpen() }}>
                <EditIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Give coins">
              <IconButton aria-label="give coin" onClick={() => { setMultiAction("giveCoin"); handleDialogClickOpen() }}>
                <MonetizationOnIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Deduct coins">
              <IconButton aria-label="draw coin" onClick={() => { setMultiAction("drawCoin"); handleDialogClickOpen() }}>
                <MoneyOffIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Delete" onClick={() => { setMultiAction("delete"); handleDialogClickOpen() }}>
              <IconButton aria-label="delete">
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          </>
        ) : ""}
      </Toolbar>
    );
  };

  function EnhancedTableHead(props: EnhancedTableProps) {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };
  
    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{ 'aria-label': 'select all desserts' }}
            />
          </TableCell>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align="center"
              padding='none'
              sortDirection={orderBy === headCell.id ? order : false}
              className={currentAdmin?.theme === "darkMode" ? classes.darkMode :""}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
    );
  }

  return (
    <div className={classes.root}>
      <Paper className={currentAdmin.theme === "darkMode" ? classes.darkModePaper :classes.paper}>
        <EnhancedTableToolbar numSelected={selected.length} />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.userID);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.userID}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                          onClick={(event) => handleClick(event, row.userID, index)}
                          disabled={currentAdmin.role === "Intern" ? true : false}
                        />
                      </TableCell>
                      <TableCell component="th" id={labelId} scope="row" padding="none" align="center" onClick={(event) => openUserInfo(row.userID)} className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>
                        {row.userID}
                      </TableCell>
                      <TableCell align="center" padding="none" onClick={(event) => openUserInfo(row.userID)} className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>{row.username}</TableCell>
                      <TableCell align="center" padding="none" onClick={(event) => openUserInfo(row.userID)} className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>{row.email}</TableCell>
                      <TableCell align="center" padding="none" onClick={(event) => openUserInfo(row.userID)} className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>{row.coins}</TableCell>
                      <TableCell align="center" padding="none" onClick={(event) => openUserInfo(row.userID)} className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>{row.lastOnline}</TableCell>
                      <TableCell>
                        <IconButton aria-label="expand row" size="small" onClick={() => openUserInfo(row.userID)}>
                          <KeyboardArrowRightIcon />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
          className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}
        />
      </Paper>
      <Dialog open={dialogOpen} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
        {
          multiAction === "delete"
          ?<>
            <Alert
              severity="warning"
              action={
                <Button variant="contained" color="secondary" size="small" onClick={deleteMultiUser}>
                  Confirm
                </Button>
              }
            >
              The selected users will be deleted.
            </Alert>
          </>
          :<>
            <DialogTitle id="form-dialog-title">
              {multiAction === "username" 
                ?"Edit Username" 
                : multiAction === "giveCoin"
                  ?"Give Coins"
                  :"Deduct Coins"
              }
            </DialogTitle>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                id="newUsername"
                label={multiAction === "username" ?"New username" : "Coins"}
                InputProps={multiAction === "username" ?{} :{
                  startAdornment: <InputAdornment position="start">$</InputAdornment>,
                  inputProps: { min: 0, max: 9999 }
                }}
                type={multiAction === "username" ?"" :"number"}
                required
                fullWidth
              />
              {multiAction !== "username"
              ?<TextField
                autoFocus
                margin="dense"
                id="coinDescription"
                label="Coin action description"
                type=""
                required
                fullWidth
              />
              :""
              }
            </DialogContent>
            <DialogActions>
              <Button onClick={editMultiUsername} color="primary">
                Confirm
              </Button>
              <Button onClick={handleDialogClose} color="primary">
                Cancel
              </Button>
            </DialogActions>
          </>
        }
      </Dialog>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={alertOpen}
        onClose={handleAlertClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={alertOpen}>
          <Alert severity="warning">
            There are insufficient coins — <strong>check it out!</strong>
          </Alert>
        </Fade>
      </Modal>
    </div>
  );
}