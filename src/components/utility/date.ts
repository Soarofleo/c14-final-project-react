export default function dateConversion(number: number): string{
  let date: any = new Date();
  let newDate = new Date(date - (number*86400000))
  return ((newDate.toDateString()).slice(4, -5))
}

export function dateToInputType(date: string | null){
  if (date === null){return}
  let newDate = date.slice(0, 11)
  let month = newDate.slice(5, 8)
  let monthDigit;
  switch (month){
    case "Jan":
      monthDigit = '01';
      break
    case "Feb":
      monthDigit = '02';
      break
    case "Mar":
      monthDigit = '03';
      break
    case "Apr":
      monthDigit = '04';
      break
    case "May":
      monthDigit = '05';
      break
    case "Jun":
      monthDigit = '06';
      break
    case "Jul":
      monthDigit = '07';
      break
    case "Aug":
      monthDigit = '08';
      break
    case "Sep":
      monthDigit = '09';
      break
    case "Oct":
      monthDigit = '10';
      break
    case "Nov":
      monthDigit = '11';
      break
    case "Dec":
      monthDigit = '12';
      break
  }
  let dateConverted = newDate.replace(month, monthDigit as string).replace(" ", "-").replace(" ", "-")
  return dateConverted
}