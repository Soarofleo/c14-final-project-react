export default function NoMatch(){

  return (
    <>
      <h1 style={{margin: "5rem"}}>404 Not Found</h1>
    </>
  )
}