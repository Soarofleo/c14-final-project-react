import { CircularProgress, createStyles, makeStyles, Theme } from "@material-ui/core";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAdminDataThunk } from "../../redux/admin/thunk";
import { IRootState } from "../../redux/store";
import Password from "./Password";
import Preferences from "./Preferences";
import Profile from "./Profile";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);



export default function Setting(){
  const classes = useStyles();
  const dispatch = useDispatch();
  const currentAdmin = useSelector((state: IRootState) => state.login.isLogin === false ? null :state.login.currentAdmin);
  const themeColor = useSelector((state: IRootState) => state.login.isLogin === true ? state.login.currentAdmin.theme : "lightBlue");

  useEffect(()=>{
    dispatch(getAdminDataThunk())
  }, [dispatch])
  
  if (!themeColor){
    return (<CircularProgress />)
  }

  return (
    <main className={classes.content}>    
      <div className={classes.toolbar} />
      <Profile/>
      <Preferences theme={themeColor}/>
      {currentAdmin?.role === "Intern" ?"" :<Password theme={themeColor}/>}
    </main>
  )
}