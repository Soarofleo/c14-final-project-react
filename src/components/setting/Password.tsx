import { Box, Button, Card, CardContent, CardHeader, Divider, Grid, makeStyles, TextField } from '@material-ui/core';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { updatePasswordThunk } from '../../redux/admin/thunk';

const useStyles = makeStyles({
  saveBtn: {
    display: 'flex',
    justifyContent: 'flex-end',
    p: 2,
    margin: 20,
  },
  darkModeEffect: {
    backgroundColor: "grey",
    color: "white",
  }
});

type Props = {
  theme: string
}

export default function Password(props: Props){
  const classes = useStyles();
  const dispatch = useDispatch();
  const [oldPasswordStatus, setOldPasswordStatus] = useState(false)
  const [newPasswordStatus, setNewPasswordStatus] = useState(false)
  const [oldAndNewPasswordStatus, setOldAndNewPasswordStatus] = useState(false)
  const [passwordConfirmationStatus, setPasswordConfirmationStatus] = useState(false)

  function handleSubmit(event: any){
    event.preventDefault()
    if (event.target.oldPassword.value === ""){
      setOldPasswordStatus(true)
      return
    }
    if (event.target.newPassword.value === ""){
      setNewPasswordStatus(true)
      return
    } 
    if (event.target.newPassword.value === event.target.oldPassword.value){
      setOldAndNewPasswordStatus(true)
      return
    }
    if (event.target.newPassword.value !== event.target.confirmation.value){
      setPasswordConfirmationStatus(true)
      return
    } else {
      dispatch(updatePasswordThunk(event.target.newPassword.value, event.target.oldPassword.value))
      event.target.reset();
    }
  }

  function handleChange(){
    if (oldPasswordStatus === true){
      setOldPasswordStatus(false)
    }
    if (newPasswordStatus === true){
      setNewPasswordStatus(false)
    }
    if (oldAndNewPasswordStatus === true){
      setOldAndNewPasswordStatus(false)
    }
    if (passwordConfirmationStatus === true){
      setPasswordConfirmationStatus(false)
    }
  }

  return (
    <form
      autoComplete="off"
      noValidate
      onSubmit={handleSubmit}
    >
      <Card className={props.theme === "darkMode" ? classes.darkModeEffect :""}>
        <CardHeader
          subheader="Update password"
          title="Password"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            // direction="row"
            // justify="center"
            // alignItems="center"
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                error={oldPasswordStatus}
                fullWidth
                helperText={oldPasswordStatus === true ? "Original password is required!" : ""}
                label="Original password"
                name="oldPassword"
                onChange={handleChange}
                type="password"
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                error={oldAndNewPasswordStatus ? true : newPasswordStatus ? true : false}
                fullWidth
                helperText={oldAndNewPasswordStatus === true ? "New password must be different from original password!" : newPasswordStatus === true ? "New password is required!" : ""}
                label="New password"
                name="newPassword"
                onChange={handleChange}
                type="password"
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                error={passwordConfirmationStatus}
                fullWidth
                helperText={passwordConfirmationStatus === true ? "Password and confirmation are not matched!" : ""}
                label="Password confirmation"
                name="confirmation"
                onChange={handleChange}
                type="password"
                variant="outlined"
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box
          className={classes.saveBtn}
        >
          <Button
            color="primary"
            type="submit"
            variant="contained"
          >
            Update
          </Button>
        </Box>
      </Card>
    </form>  
  )
}