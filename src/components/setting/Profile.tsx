import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import { Box, CardHeader, CircularProgress, Divider, Grid, TextField } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { updateAdminThunk } from '../../redux/admin/thunk';

const useStyles = makeStyles({
  saveBtn: {
    display: 'flex',
    justifyContent: 'flex-end',
    p: 2,
    margin: 20,
  },
  darkMode: {
    backgroundColor: "grey",
    color: "white",
  }
});


function handleChange(){

}

export default function Profile(){
  const classes = useStyles();
  const statusType = useSelector((state: IRootState) => state.admin.type);
  const currentAdmin = useSelector((state: IRootState) => state.login.isLogin === false ? null :state.login.currentAdmin);
  const dispatch = useDispatch();
  if (statusType === "loading"){return (<CircularProgress />)}
  if (!currentAdmin){return <p>Failed to load</p>}

  function handleSubmit(event: any){
    event.preventDefault()
    let data = {adminName: event.target.adminName.value, email: event.target.email.value, role: event.target.role.value}
    if (event.target.adminName.value === currentAdmin?.adminName && event.target.email.value === currentAdmin?.email && event.target.role.value === currentAdmin?.role){
      return
    }
    if (event.target.adminName.value === currentAdmin?.adminName){
      delete data.adminName
    }
    if (event.target.email.value === currentAdmin?.email){
      delete data.email
    }
    if (event.target.role.value === currentAdmin?.role){
      delete data.role
    }
    dispatch(updateAdminThunk(data))
  }

  return (
    <form
      autoComplete="off"
      noValidate
      onSubmit={handleSubmit}
    >
      <Card className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>
        <CardHeader
          subheader={currentAdmin.role === "Intern" ? "Intern is not allowed to edit the profile" : "The information can be edited"}
          title="Profile"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            // direction="row"
            // justify="center"
            // alignItems="center"
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Admin name"
                name="adminName"
                onChange={handleChange}
                required
                defaultValue={currentAdmin.adminName}
                variant="outlined"
                disabled={currentAdmin["role"] === "Intern" ? true : false}
              />
            </Grid>
            <Grid
              item
              // justify="center"
              // alignItems="center"
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Email Address"
                name="email"
                onChange={handleChange}
                required
                defaultValue={currentAdmin.email}
                variant="outlined"
                disabled={currentAdmin["role"] === "Intern" ? true : false}
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Role"
                name="role"
                helperText="Only Super Admin can alter self role for once"
                onChange={handleChange}
                required
                select
                SelectProps={{ native: true }}
                defaultValue={currentAdmin.role}
                variant="outlined"
                disabled={currentAdmin["role"] === "Super" ? false : true}
              >
                <option value="Super">Super Admin</option>
                <option value="Ordinary">Ordinary Admin</option>
                <option value="Intern">Intern</option>
              </TextField>
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box
          className={classes.saveBtn}
        >
          {
            currentAdmin.role === "Intern"
            ?""
            :<Button
              color="primary"
              type="submit"
              variant="contained"
            >
              Save details
            </Button>
          }
        </Box>
      </Card>
    </form>  
  );
}