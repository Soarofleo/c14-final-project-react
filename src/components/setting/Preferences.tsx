import { Card, CardContent, CardHeader, Divider, FormControl, FormControlLabel, Grid, makeStyles, Radio, RadioGroup, RadioProps } from "@material-ui/core";
import React, { useState } from "react";
import clsx from 'clsx';
import { useDispatch } from "react-redux";
import { updateThemeThunk } from "../../redux/admin/thunk";

const useStyles = makeStyles({
  form: {
    marginTop: 20,
    marginBottom: 20,
  },
  grid: {
    md: 4,
    sm: 6,
    sx: {
      display: 'flex',
      flexDirection: 'column'
    },
    xs: 12,
  },
  box: {
    display: 'flex',
    justifyContent: 'flex-end',
    p: 2,
    margin: 20,
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#137cbd',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#106ba3',
    },
  },
  gridContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  gridItem: {
    margin: 10,
  },
  lightBlue: {
    backgroundColor: "#3f51b5",
    width: 120,
    height: 40,
    borderRadius: 10,
  },
  crimson: {
    backgroundColor: "rgb(227, 51, 113)",
    width: 120,
    height: 40,
    borderRadius: 10,
  },
  light: {
    backgroundColor: "#dfdfdf",
    width: 120,
    height: 40,
    borderRadius: 10,
  },
  dark: {
    backgroundColor: "#2e2e2e",
    width: 120,
    height: 40,
    borderRadius: 10,
  },
  darkMode: {
    backgroundColor: "#1f1f1f",
    width: 120,
    height: 40,
    borderRadius: 10,
  },
  darkModeEffect: {
    backgroundColor: "grey",
    color: "white",
  }
});

function StyledRadio(props: RadioProps) {
  const classes = useStyles();

  return (
    <Radio
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

type Props = {
  theme: string
}

export default function Preferences(props: Props){
  const classes = useStyles();
  const [themeColor, setThemeColor] = useState<string>(props.theme)
  const dispatch = useDispatch();
  

  function changeThemeColor(e: React.ChangeEvent<HTMLInputElement>){
    setThemeColor(e.target.value)
    dispatch(updateThemeThunk(e.target.value as any))
  }

  function handleSubmit(event: any){
    event.preventDefault()

  }

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
      <Card className={props.theme === "darkMode" ? classes.darkModeEffect :""}>
        <CardHeader
          subheader="Manage your theme"
          title="Themes"
        />
        <Divider />
        <CardContent>
          <FormControl component="fieldset">
            <RadioGroup value={themeColor} aria-label="gender" name="customized-radios" onChange={changeThemeColor}>
              <Grid container className={classes.gridContainer}>
                <Grid item className={classes.gridItem}>
                  <div className={classes.lightBlue}></div>
                  <FormControlLabel value="lightBlue" control={<StyledRadio />} label="Light Blue" />
                </Grid>
                <Grid item className={classes.gridItem}>
                  <div className={classes.crimson}></div>
                  <FormControlLabel value="crimson" control={<StyledRadio />} label="Crimson" />
                </Grid>
                <Grid item className={classes.gridItem}>
                  <div className={classes.light}></div>
                  <FormControlLabel value="light" control={<StyledRadio />} label="Light" />
                </Grid>
                <Grid item className={classes.gridItem}>
                  <div className={classes.dark}></div>
                  <FormControlLabel value="dark" control={<StyledRadio />} label="Dark" />
                </Grid>
              </Grid>
            </RadioGroup>
          </FormControl>
        </CardContent>
      </Card>
    </form>
  )
}