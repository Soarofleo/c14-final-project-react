import { ConnectedRouter } from 'connected-react-router';
import { history } from '../../redux/store';
import Loading from '../utility/Loading';

export default function App() {

  return (
    <ConnectedRouter history={history}>
      <Loading/>
    </ConnectedRouter>
  );
}