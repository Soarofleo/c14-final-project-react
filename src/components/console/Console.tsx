import { Backdrop, Button, Checkbox, CircularProgress, createStyles, Fade, FormControl, InputLabel, List, ListItem, ListItemIcon, ListItemText, makeStyles, Modal, Paper, Select, Snackbar, Table, TableBody, TableCell, TableContainer, TableFooter, TableHead, TablePagination, TableRow, Theme, Typography } from "@material-ui/core";
import TablePaginationActions from "@material-ui/core/TablePagination/TablePaginationActions";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { resetTableContent } from "../../redux/console/action";
import { deleteTableRowThunk, loadColumnsThunk, loadTableContentThunk, loadTablesThunk } from "../../redux/console/thunk";
import { IRootState } from "../../redux/store";
import { Alert } from "@material-ui/lab";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    table: {
      minWidth: 500,
    },
    root: {
      width: '100%',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
    paper: {
      padding: 20,
    },
    title: {
      marginTop: 20,
      marginBottom: 10,
    },
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    fade: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }),
);


export default function Console(){
  const classes = useStyles();
  const tables = useSelector((state: IRootState) => state.console.tables)
  const columns = useSelector((state: IRootState) => state.console.columns)
  const status = useSelector((state: IRootState) => state.console.status)
  const deleteRowResult = useSelector((state: IRootState) => state.console.deleteRowResult)
  const [tableSelected, setTableSelected] = useState("")
  const [functionType, setFunctionType] = useState<"" | "select" | "insert" | "update" | "del">("")
  const [columnSelected, setColumnSelected] = useState<string[]>([])
  const tableContent = useSelector((state: IRootState) => state.console.content)
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [rowSelected, setRowSelected] = useState<null | number>(null)
  const [rowIDSelected, setRowIDSelected] = useState<null | number>(null)
  const [reminderForDeletion, setReminderForDeletion] = useState(false)
  const [modalOpen, setModalOpen] = useState(false);
  const [snackBarOpen, setSnackBarOpen] = useState(false);

  const dispatch = useDispatch()

  useEffect(()=>{
    dispatch(loadTablesThunk())
  }, [dispatch])

  useEffect(()=>{
    dispatch(loadColumnsThunk(tableSelected))
  }, [dispatch, tableSelected])

  useEffect(()=>{
    setReminderForDeletion(false)
  }, [rowIDSelected, tableSelected, functionType])

  useEffect(()=>{
    if (deleteRowResult === true){
      setSnackBarOpen(true)
    }
  }, [deleteRowResult])

  useEffect(() => {
    setTimeout(()=>{
      setSnackBarOpen(false);
    }, 2000)
  }, [snackBarOpen]);

  const handleChange = (event: React.ChangeEvent<{ name?: string; value: any }>) => {
    setTableSelected(event.target.value)
    setColumnSelected([])
    dispatch(resetTableContent())
  }
  const handleFunctionChange = (event: React.ChangeEvent<{ name?: string; value: any }>) => {
    setFunctionType(event.target.value)
  }
  const handleColumnChange = (column: string) => {
    if (columnSelected.indexOf(column) === -1){
      setColumnSelected([...columnSelected, column])
    } else {
      let newArray = [...columnSelected]
      newArray.splice(columnSelected.indexOf(column), 1)
      setColumnSelected(newArray)
    }
  }
  
  function submitRequest() {
    if (functionType === 'select' && columnSelected.length > 0){
      setFunctionType("")
      setRowSelected(null)
      setRowIDSelected(null)
      dispatch(loadTableContentThunk(tableSelected))
    }
  }

  const handleChangePage = (event: any, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: any) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  function clickRow(index: number, item: any){
    if(rowSelected !== page*rowsPerPage - rowsPerPage + index){
      setRowSelected(page*rowsPerPage - rowsPerPage + index)
      if (item.id){
        setRowIDSelected(item.id)
      }
    } else {
      setRowSelected(null)
      setRowIDSelected(null)
    }
  }

  function openWarning(){
    if (rowIDSelected === null){
      setReminderForDeletion(true)
    } else {
      setModalOpen(true)
    }
  }

  function deleteRow(){
    if (rowIDSelected === null){return}
    setModalOpen(false)
    dispatch(deleteTableRowThunk(tableSelected, rowIDSelected))
  }


  function columnSelection() {
    return (
      <List className={classes.root}>
      {columns && columns.map((item, index) => {
        const labelId = `checkbox-list-label-${item}`;
        return (
        <ListItem key={"column" + index} role={undefined} dense button onClick={()=>handleColumnChange(item.column_name)}>
          <ListItemIcon>
            <Checkbox
              edge="start"
              checked={columnSelected.indexOf(item.column_name) !== -1}
              tabIndex={-1}
              disableRipple
              inputProps={{ 'aria-labelledby': labelId }}
            />
          </ListItemIcon>
          <ListItemText id={labelId} primary={item.column_name} />
        </ListItem>
        )
        })}
      </List>
    )
  }

  return (
    <main className={classes.content}>    
      <div className={classes.toolbar} />
      {status === 'loading'
      ?<CircularProgress />
      :status === 'error'
      ?<p>There is error, please note that only super admin is allowed to entry this page. If you are super admin, please try to refresh the page.</p>
      :<Paper className={classes.paper}>
        <Typography variant="h5" className={classes.title}>
          Admin Console
        </Typography>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel htmlFor="outlined-age-native-simple">Tables</InputLabel>
          <Select
            native
            defaultValue={tableSelected}
            onChange={handleChange}
            label="Tables"
            inputProps={{
              name: 'Tables',
              id: 'Tables',
            }}
          >
            <option aria-label="None" value="" />
            {tables?.map((item, index)=>
              item.table_name === 'knex_migrations' || item.table_name === 'knex_migrations_lock' 
              ? null
              :<option key={'table' + index} value={item.table_name}>{item.table_name}</option>
            )}
          </Select>
        </FormControl>
        { tableSelected === ""
        ? null
        :<FormControl variant="outlined" className={classes.formControl}>
          <InputLabel htmlFor="outlined-age-native-simple">Functions</InputLabel>
          <Select
            native
            value={functionType}
            onChange={handleFunctionChange}
            label="Functions"
            inputProps={{
              name: 'Functions',
              id: 'Functions',
            }}
          >
            <option aria-label="None" value="" />
            <option value="select">Select</option>
            <option value="insert">Insert</option>
            <option value="update">Update</option>
            <option value="del">Delete</option>
          </Select>
        </FormControl>
        }
        {functionType === ""
          ? null
          : functionType === "select"
          ? columnSelection()
          : functionType === "insert"
          ? <span>Not yet supported</span>
          : functionType === "update"
          ? <span>Not yet supported</span>
          : functionType === "del"
          ? <div>
              <Button variant="contained" color="primary" onClick={openWarning}>
                Deletion
              </Button>
            </div>
          : null
        }
        {reminderForDeletion &&
          <span style={{color: 'red'}}>Please get a list of table and select row for deletion</span>
        }
        {columnSelected.length !== 0 && functionType === "select"
        ?<div>
          <Button variant="contained" color="primary" onClick={submitRequest}>
            Submit
          </Button>
        </div>
        :null
        }
        <Typography variant="h5" className={classes.title}>
          Result:
        </Typography>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="custom pagination table">
            <TableHead>
              <TableRow>
                {columnSelected.includes("*") && columns
                ?columns.map((column, index) => (
                  <TableCell
                    key={column.column_name + index}
                    style={{ width: 160 }} 
                    align="left"
                  >
                    {column.column_name}
                  </TableCell>
                ))
                :columnSelected.map((column, index) => (
                  <TableCell
                    key={column + index}
                    style={{ width: 160 }} 
                    align="left"
                  >
                    {column}
                  </TableCell>
                ))
                }
              </TableRow>
            </TableHead>
            <TableBody>
              {rowsPerPage && rowsPerPage > 0 && tableContent &&
               tableContent.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, index)=>{
                  return (
                    <TableRow 
                      hover={item.id !== undefined}
                      selected={(page*rowsPerPage - rowsPerPage + index) === rowSelected}
                      onClick={()=>clickRow(index, item)}
                      key={'row' + index}
                    >
                      {columnSelected.map((column, num)=>{
                        let value = item[column]
                        if (typeof(value) === 'object'){
                          value = JSON.stringify(value)
                        } else if (typeof(value) === 'boolean'){
                          if (value === true){
                            value = 'yes'
                          } else if (value === false){
                            value = 'no'
                          }
                        } 
                        return (
                          <TableCell key={'row' + index + 'column' + num} style={{ width: 160 }} align="left">
                            {value}
                          </TableCell>
                        )
                      })}
                    </TableRow>
                  );
                })
              }
            </TableBody>
            <TableFooter>
              <TableRow>
                <TableCell style={{ width: 160 }} align="left">
                  Total row: {tableContent?.length}
                </TableCell>
                <TableCell style={{ width: 160, color: 'red', fontSize: 16 }} align="left">
                  {rowSelected !== null && rowIDSelected !== null && `ID ${rowIDSelected} is selected`}
                </TableCell>
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                  colSpan={columnSelected.length -2}
                  count={tableContent ?tableContent.length :0}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  SelectProps={{
                    inputProps: { 'aria-label': 'rows per page' },
                    native: true,
                  }}
                  onChangePage={handleChangePage}
                  onChangeRowsPerPage={handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
        <Modal
          aria-labelledby="spring-modal-title"
          aria-describedby="spring-modal-description"
          className={classes.modal}
          open={modalOpen}
          onClose={()=>setModalOpen}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={modalOpen}>
            <div className={classes.fade}>
              <h2 style={{color: 'red'}}>Warning</h2>
              <p>All items in row (ID: {rowIDSelected}) of {tableSelected} will be deleted. Please confirm!</p>
              <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                <Button variant="contained" color="secondary" onClick={deleteRow}>
                  Confirm
                </Button>
                <Button style={{marginLeft: 20}} variant="contained" color="primary" onClick={()=>setModalOpen(false)}>
                  Cancel
                </Button>
              </div>
            </div>
          </Fade>
        </Modal>
        <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          open={snackBarOpen}
        >
          <Alert severity="success">
            This is a success message!
          </Alert>
        </Snackbar>
      </Paper>
      }
     </main>
  )
}