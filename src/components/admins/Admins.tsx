import { createStyles, makeStyles, Theme } from "@material-ui/core";
import { useSelector } from "react-redux";
import { IRootState } from "../../redux/store";
import AdminList from "./AdminList";
import AdminRemark from "./AdminRemark";


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

export default function Admins(){
  const classes = useStyles();
  const currentAdmin = useSelector((state: IRootState) => state.login.isLogin === false ? null :state.login.currentAdmin);

  return (
    <main className={classes.content}>    
      <div className={classes.toolbar} />
      <AdminList/>
      <AdminRemark theme={currentAdmin?.theme}/>
    </main>
  )
}
