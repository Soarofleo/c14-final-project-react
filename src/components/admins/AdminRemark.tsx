import { createStyles, lighten, makeStyles, Paper, Table, TableBody, TableCell, TableHead, TableRow, Theme, Toolbar, Typography } from "@material-ui/core";

const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    paper: {
      marginTop: 15,
    },
    darkModePaper: {
      marginTop: 15,
      backgroundColor: "grey",
      color: "white",
    },
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
    },
    darkModeEffect: {
      backgroundColor: "grey",
      color: "white",
    }
  }),
);

type Props = {
  theme: any
}

export default function AdminRemark(props: Props){
  const classes = useToolbarStyles();
  return (
    <>
      <Paper className={props.theme === "darkMode" ? classes.darkModePaper :classes.paper}>
        <Toolbar
          className={classes.root}
        >
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            Remarks
          </Typography>
        </Toolbar>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell className={props.theme === "darkMode" ? classes.darkModeEffect :""}>
                Control:
              </TableCell>
              <TableCell className={props.theme === "darkMode" ? classes.darkModeEffect :""}>
                can create new administrator, edit and delete role and profile of administrators other than super administrator, restore reply status of reports
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell className={props.theme === "darkMode" ? classes.darkModeEffect :""}>
                Edit:
              </TableCell>
              <TableCell className={props.theme === "darkMode" ? classes.darkModeEffect :""}>
                can edit self profile, edit all users' profile and update reply status of reports
              </TableCell>
            </TableRow>
          </TableBody>
          <TableBody>
            <TableRow>
              <TableCell className={props.theme === "darkMode" ? classes.darkModeEffect :""}>
                View:
              </TableCell>
              <TableCell className={props.theme === "darkMode" ? classes.darkModeEffect :""}>
                can view record and set self theme
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    </>
  )
}
