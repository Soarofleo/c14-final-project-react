import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControl, FormHelperText, InputLabel, MenuItem, Select, TextField } from "@material-ui/core";
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import React from "react";
import { useDispatch } from "react-redux";
import { addAdminThunk } from "../../redux/admin/thunk";


export default function AdminControl(){
  const [open, setOpen] = React.useState(false);
  const [role, setRole] = React.useState<null | "Super" | "Ordinary" | "Intern">(null);
  const [emailError, setEmailError] = React.useState(false);
  const [nameError, setNameError] = React.useState(false);
  const [passwordError, setPasswordError] = React.useState(false);
  const [roleError, setRoleError] = React.useState(false);
  const dispatch = useDispatch();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setEmailError(false)
    setNameError(false)
    setPasswordError(false)
    setRoleError(false)
    setRole(null)
  };

  const handleSelectChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    if (event.target.value === "Super" || event.target.value ===  "Ordinary" || event.target.value === "Intern")
    setRole(event.target.value);
    setRoleError(false)
  };

  function handleChange(){
    setEmailError(false)
    setNameError(false)
    setPasswordError(false)
    setRoleError(false)
  }

  function handleSubmit(event: any){
    event.preventDefault()
    if (event.target.email.value === ""){
      setEmailError(true)
      return
    }
    if (event.target.adminName.value === ""){
      setNameError(true)
      return
    }
    if (event.target.password.value === ""){
      setPasswordError(true)
      return
    }
    if (role === null){
      setRoleError(true)
      return
    }
    dispatch(addAdminThunk(
      event.target.email.value,
      event.target.adminName.value,
      event.target.password.value,
      role))
    handleClose();
  }

  return (
    <>
      <Button color={"primary"} variant={"contained"} startIcon={<AddCircleOutlineIcon/>} onClick={handleClickOpen}>
        Administrator
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add a new administrator</DialogTitle>
        <form onSubmit={handleSubmit} id="adminControlForm">
          <DialogContent>
            <DialogContentText>
              Please fill in the information of the new administrator.
            </DialogContentText>
              <TextField
                autoFocus
                margin="dense"
                id="email"
                label="Email Address"
                name="email"
                type="email"
                fullWidth
                onChange={handleChange}
                error={emailError}
                helperText={emailError ? "Email is required" : ""}
              />
              <TextField
                autoFocus
                margin="dense"
                id="adminName"
                label="Admin Name"
                name="adminName"
                type=""
                fullWidth
                onChange={handleChange}
                error={nameError}
                helperText={nameError ? "Admin name is required" : ""}
              />
              <TextField
                autoFocus
                margin="dense"
                id="password"
                label="Password"
                name="password"
                type=""
                fullWidth
                onChange={handleChange}
                error={passwordError}
                helperText={passwordError ? "Password is required" : ""}
              />
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-helper-label" error={roleError} >Role</InputLabel>
              <Select
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                value={role}
                onChange={handleSelectChange}
              >
                <MenuItem value={"Super"}>Super Admin</MenuItem>
                <MenuItem value={"Ordinary"}>Ordinary Admin</MenuItem>
                <MenuItem value={"Intern"}>Intern</MenuItem>
              </Select>
              <FormHelperText error={roleError} >{roleError ? "Role is required" : ""}</FormHelperText>
            </FormControl>
          </DialogContent>
          <DialogActions>
          <Button type="submit" color="primary" variant={"contained"}>
              Submit
            </Button>
            <Button onClick={handleClose} color="secondary" variant={"contained"}>
              Cancel
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </>
  )
}