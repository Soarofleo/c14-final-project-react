import React, { useEffect } from 'react';
import clsx from 'clsx';
import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import EditIcon from '@material-ui/icons/Edit';
import { deleteMultiAdminThunk, editMultiAdminThunk, getAdminDataThunk } from '../../redux/admin/thunk';
import { Button, CircularProgress, Dialog, NativeSelect, TextField } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import AdminControl from './AdminControl';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';

interface Data {
  adminID: number;
  adminName: string;
  email: string;
  role: "Super" | "Ordinary" | "Intern";
  authority: "Super" | "Ordinary" | "Intern";
}


function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key,
): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
}

const headCells: HeadCell[] = [
  { id: 'adminID', numeric: true, disablePadding: false, label: 'Admin ID' },
  { id: 'adminName', numeric: false, disablePadding: true, label: 'Admin Name' },
  { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
  { id: 'role', numeric: false, disablePadding: false, label: 'Role' },
  { id: 'authority', numeric: false, disablePadding: false, label: 'Authority' },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  numSelected: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}



const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
          },
    title: {
      flex: '1 1 100%',
    },
  }),
);

interface EnhancedTableToolbarProps {
  numSelected: number;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    darkModePaper: {
      width: '100%',
      marginBottom: theme.spacing(2),
      backgroundColor: "grey",
      color: "white",
    },
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
    btn: {
      '& > *': {
        margin: theme.spacing(0.5),
        color: "black !important"
      },
    },
    darkModeBtn: {
      '& > *': {
        margin: theme.spacing(0.5),
        color: "white !important"
      },
    },
    currentAdmin: {
      backgroundColor: "gainsboro"
    },
    darkMode: {
      backgroundColor: "grey",
      color: "white",
    }
  }),
);

export default function AdminList() {
  const classes = useStyles();
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof Data>('adminID');
  const [selected, setSelected] = React.useState<number[]>([]);
  const [indexes, setIndexes] = React.useState<number[]>([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [dialogOpen, setDialogOpen] = React.useState(false);
  const [editMode, setEditMode] = React.useState(false);
  const [editContent, setEditContent] = React.useState<any[]>([])
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(getAdminDataThunk())
  }, [dispatch])
  
  const data = useSelector((state: IRootState) => state.admin.type === "loading" ? null : state.admin.data);
  const currentAdmin = useSelector((state: IRootState) => state.login.isLogin === false ? null :state.login.currentAdmin);
  if (!data){return (<CircularProgress />)}
  if (!currentAdmin){return (<CircularProgress />)}
  const rows = data.adminData

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof Data) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (currentAdmin.role !== "Super"){
      return
    }
    if (selected.length === 0) {
      const newSelecteds = []
      const newIndexes = []
      for (let i = 0; i < rows.length; i++){
        if (rows[i].adminID !== currentAdmin.adminID && rows[i].role !== "Super"){
          newSelecteds.push(rows[i].adminID)
          newIndexes.push(i)
        }
      }
      setSelected(newSelecteds);
      setIndexes(newIndexes)
      return;
    }
    setSelected([]);
    setIndexes([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, adminID: number, index: number) => {
    const selectedIndex = selected.indexOf(adminID);
    let newSelected: number[] = [];
    let newIndexes: number[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, adminID);
      newIndexes = newIndexes.concat(indexes, index);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
      newIndexes = newIndexes.concat(indexes.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
      newIndexes = newIndexes.concat(indexes.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
      newIndexes = newIndexes.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
    setIndexes(newIndexes);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const isSelected = (name: number) => selected.indexOf(name) !== -1;


  const emptyRows = rowsPerPage - Math.min(rowsPerPage, data!.adminData.length - page * rowsPerPage);

  const handleDialogClickOpen = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  function deleteMultiAdmin(){
    setSelected([]);
    setIndexes([]);
    setDialogOpen(false);
    dispatch(deleteMultiAdminThunk(selected, indexes))
  }

  function cancelEdition(){
    setEditMode(false)
    setEditContent([])
  }

  function submitEdition(){
    if (editContent.length === 0){
      setEditMode(false)
      setSelected([])
      return
    }
    dispatch(editMultiAdminThunk(editContent))
    setTimeout(()=>{
      setEditMode(false)
      setSelected([])
    }, 1500)
  }

  const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
    const classes = useToolbarStyles();
    const { numSelected } = props;
  
    return (
      <Toolbar
        className={clsx(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        {numSelected > 0 ? (
          <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            List of Administrators
          </Typography>
        )}
        {numSelected > 0 ? (
          <>
          { editMode === false
            ?<>
              <Tooltip title="Edit" onClick={()=>{setEditMode(true)}}>
                <IconButton aria-label="edit">
                  <EditIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Delete" onClick={() => { handleDialogClickOpen() }}>
                <IconButton aria-label="delete">
                  <DeleteIcon />
                </IconButton>
              </Tooltip>
            </>
            :<>
              <Tooltip title="Cancel" onClick={cancelEdition}>
                <IconButton aria-label="cancel">
                  <CancelIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Save" onClick={submitEdition}>
                <IconButton aria-label="save">
                  <SaveIcon />
                </IconButton>
              </Tooltip>
            </>
          }
          </>
        ) : ""}
      </Toolbar>
    );
  };
  
  function EnhancedTableHead(props: EnhancedTableProps) {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };
  
    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{ 'aria-label': 'select all desserts' }}
            />
          </TableCell>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align="center"
              padding='none'
              sortDirection={orderBy === headCell.id ? order : false}
              className={currentAdmin?.theme === "darkMode" ? classes.darkMode :""}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }

  function saveEditContent(event: any){
    let object: any;
    for (let i = 0; i < editContent.length; i++){
      if (editContent[i].id === parseInt(event.target.id)){
        object = editContent[i]
        if (event.target.name === "Email"){
          object.email = event.target.value
        }
        if (event.target.name === "Admin Name"){
          object.adminName = event.target.value
        }
        if (event.target.name === "Role"){
          object.role = event.target.value
        }
        let newArray = [...editContent]
        newArray[i] = object
        setEditContent(newArray)
        return
      }
    }
    object = {id: parseInt(event.target.id), email: null, adminName: null, role: null}
    if (event.target.name === "Email"){
      object.email = event.target.value
    }
    if (event.target.name === "Admin Name"){
      object.adminName = event.target.value
    }
    if (event.target.name === "Role"){
      object.role = event.target.value
    }
    let newArray = [...editContent]
    newArray.push(object)
    setEditContent(newArray)
  }

  return (
    <div className={classes.root}>
      <Paper className={currentAdmin.theme === "darkMode" ? classes.darkModePaper : classes.paper}>
        <EnhancedTableToolbar numSelected={selected.length} />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.adminID);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.adminID}
                      selected={isItemSelected}
                      className={row.adminID === currentAdmin.adminID ?classes.currentAdmin :""}
                    >
                      <TableCell padding="checkbox">
                        <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                          onClick={(event) => handleClick(event, row.adminID, index)}
                          disabled={row.adminID === currentAdmin.adminID || row.role === "Super" ?true : currentAdmin.role === "Super" ? false : true}
                        />
                      </TableCell>
                      <TableCell component="th" id={labelId} scope="row" padding="none" align="center" className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>
                        {row.adminID}
                      </TableCell>
                      <TableCell align="center" padding="none" className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>
                        {editMode === true && selected.indexOf(row.adminID) !== -1 
                        ?<TextField 
                          id={row.adminID.toString()} 
                          name="Admin Name"
                          variant="filled" 
                          defaultValue={row.adminName} 
                          onChange={saveEditContent}/> 
                        :row.adminName}
                      </TableCell>
                      <TableCell align="center" padding="none" className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>
                        {editMode === true && selected.indexOf(row.adminID) !== -1 
                        ?<TextField 
                          id={row.adminID.toString()}
                          name="Email"
                          variant="filled" 
                          defaultValue={row.email} 
                          onChange={saveEditContent}/> 
                        :row.email}
                      </TableCell>
                      <TableCell align="center" padding="none" className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>
                        {editMode === true && selected.indexOf(row.adminID) !== -1 
                        ?<NativeSelect
                          defaultValue={row.role}
                          name="Role"
                          inputProps={{id: row.adminID.toString()}}
                          onChange={saveEditContent}
                        >
                          <option value={"Super"}>Super Admin</option>
                          <option value={"Ordinary"}>Ordinary Admin</option>
                          <option value={"Intern"}>Intern</option>
                        </NativeSelect>
                        :row.role}
                      </TableCell>
                      <TableCell align="center" padding="none" className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}>
                        {row.authority === "Super" 
                        ? <div className={currentAdmin.theme === "darkMode" ? classes.darkModeBtn :classes.btn}>
                            <Button variant="contained" disabled>Control</Button>
                            <Button variant="contained" disabled>Edit</Button>
                            <Button variant="contained" disabled>View</Button>
                          </div>
                        : <>{row.authority === "Ordinary"
                          ? <div className={currentAdmin.theme === "darkMode" ? classes.darkModeBtn :classes.btn}>
                              <Button variant="contained" disabled>Edit</Button>
                              <Button variant="contained" disabled>View</Button>
                            </div>
                          : <div className={currentAdmin.theme === "darkMode" ? classes.darkModeBtn :classes.btn}>
                              <Button variant="contained" disabled>View</Button>
                            </div>
                          }</>}
                      </TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
          className={currentAdmin.theme === "darkMode" ? classes.darkMode :""}
        />
      </Paper>
      {currentAdmin?.role === "Super" ? <AdminControl/> : ""}
      <Dialog open={dialogOpen} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
        <Alert
          severity="warning"
          action={
            <Button variant="contained" color="secondary" size="small" onClick={deleteMultiAdmin}>
              Confirm
            </Button>
          }
        >
          The selected administrators will be deleted.
        </Alert>
      </Dialog>
    </div>
  );
}