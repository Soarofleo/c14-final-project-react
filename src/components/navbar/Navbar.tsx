import React, { useEffect, useState } from 'react';
import { makeStyles, Theme, createStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import clsx from 'clsx';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import './Navbar.css'
import { Link, LinkProps, Route, Switch } from 'react-router-dom';
import Dashboard from '../dashboard/Dashboard';
import NoMatch from '../noMatch/NoMatch';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import DashboardIcon from '@material-ui/icons/Dashboard';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import SettingsIcon from '@material-ui/icons/Settings';
import FaceIcon from '@material-ui/icons/Face';
import Reports from '../reports/Reports';
import Users from '../users/Users';
import { useDispatch, useSelector } from 'react-redux';
import Admins from '../admins/Admins';
import Setting from '../setting/Setting';
import { getReportDataThunk } from '../../redux/report/thunk';
import { logout } from '../../redux/login/action';
import { Button, Dialog } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { IRootState } from '../../redux/store';
import Console from '../console/Console';
import GamesIcon from '@material-ui/icons/Games';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    darkModeRoot: {
      display: 'flex',
      backgroundColor: "dimgrey",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    crimsonAppBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      backgroundColor: "crimson"
    },
    lightAppBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      backgroundColor: "#dfdfdf",
      color: "black"
    },
    darkAppBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      backgroundColor: "#2e2e2e"
    },
    darkModeAppBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      backgroundColor: "#1f1f1f",
      color: "grey"
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: 36,
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
    },
    darkModeDrawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      backgroundColor: "grey",
      color: "white",
    },
    darkModeDrawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
      backgroundColor: "grey",
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    text: {
      color: "blue"
    }
  }),
);

export default function MainNavbar(){

  const classes = useStyles();
  const dispatch = useDispatch();
  useEffect(()=>{
    dispatch(getReportDataThunk())
  }, [dispatch])
  const reports = useSelector((state: IRootState) => state.report.reportData);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState<null | HTMLElement>(null);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const themeColor = useSelector((state: IRootState) => state.login.isLogin === true ? state.login.currentAdmin.theme : "lightBlue");
  const role = useSelector((state: IRootState) => state.login.isLogin === true && state.login.currentAdmin.role)
  const [dialogOpen, setDialogOpen] = React.useState(false);

  let counter: number[] = [];
  for (let i = 0; i < reports.length; i++){
    if (reports[i].isRead === "no"){
      counter.push(1)
    }
  }
  
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const mobileMenuId = 'primary-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem component={Link} to="/Reports">
        <IconButton color="inherit">
          <Badge badgeContent={counter.length} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>Notifications</p>
      </MenuItem>
      <MenuItem  component={Link} to="/Setting">
        <IconButton color="inherit">
          <AccountCircle />
        </IconButton>
        <p>Setting</p>
      </MenuItem>
    </Menu>
  );

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  interface ListItemLinkProps {
    icon?: React.ReactElement;
    className: any;
    primary: string;
    to: string;
  }

  function ListItemLink(props: ListItemLinkProps) {
    const { icon, className, primary, to } = props;
  
    const renderLink = React.useMemo(
      () =>
        React.forwardRef<any, Omit<LinkProps, 'to'>>((itemProps, ref) => (
          <Link to={to} ref={ref} {...itemProps} />
        )),
      [to],
    );
  
    return (
      <li>
        <ListItem button component={renderLink} onClick={()=>setRouteColor(primary)}>
          {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
          <ListItemText primary={primary} className={className} />
        </ListItem>
      </li>
    );
  }

  const handleDialogClickOpen = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
  };

  const [routeColor, setRouteColor] = useState("Dashboard")
  let website = window.location.href

  useEffect(()=>{
    if (window.location.href.includes("Reports")){
      setRouteColor("Reports")
    } else if (window.location.href.includes("Users")){
      setRouteColor("Users")
    } else if (window.location.href.includes("Admins")){
      setRouteColor("Admins")
    } else if (window.location.href.includes("Setting")){
      setRouteColor("Setting")
    } else if (window.location.href.includes("Console")){
      setRouteColor("Console")
    } else {
      setRouteColor("Dashboard")
    }
  }, [website])

  return (
    <div className={themeColor === "darkMode" ?classes.darkModeRoot :classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={
          themeColor === "crimson"
          ?
          clsx(classes.crimsonAppBar, {
            [classes.appBarShift]: open,
          })
          :`${themeColor === "light"
            ?clsx(classes.lightAppBar, {
              [classes.appBarShift]: open,
            })
            :`${themeColor === "dark"
              ?clsx(classes.darkAppBar, {
                [classes.appBarShift]: open,
              })
              :`${themeColor === "darkMode"
                ?clsx(classes.darkModeAppBar, {
                  [classes.appBarShift]: open,
                })
                :clsx(classes.appBar, {
                  [classes.appBarShift]: open,
                })
              }`
            }`
          }`
        }
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6" noWrap component={Link} to="/" onClick={()=>setRouteColor("Dashboard")}>
            <div className="logoContainer">
              <img className="logo" src="/Logo2.svg" alt="logo"/>
            </div>
          </Typography>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton aria-label="show 17 new notifications" color="inherit" component={Link} to="/Reports" onClick={()=>setRouteColor("Reports")}>
              <Badge badgeContent={counter.length} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <IconButton color="inherit" component={Link} to="/Setting" onClick={()=>setRouteColor("Setting")}>
              <AccountCircle />
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: `${themeColor === "darkMode"
            ?clsx({
              [classes.darkModeDrawerOpen]: open,
              [classes.darkModeDrawerClose]: !open,
            })
            :clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open,
            })
          }`,
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>
          <ListItemLink to="/Dashboard" primary="Dashboard" className={routeColor === "Dashboard" ? classes.text : ""} icon={<DashboardIcon color={routeColor === "Dashboard" ? "primary" :"action"}/>} />
          <ListItemLink to="/Reports" primary="Reports" className={routeColor === "Reports" ? classes.text : ""} icon={<MailIcon color={routeColor === "Reports" ? "primary" :"action"}/>} />
        </List>
        <Divider />
        <List>
          <ListItemLink to="/Users" primary="Users" className={routeColor === "Users" ? classes.text : ""} icon={<RecentActorsIcon color={routeColor === "Users" ? "primary" :"action"}/>} />
          <ListItemLink to="/Admins" primary="Admins" className={routeColor === "Admins" ? classes.text : ""} icon={<FaceIcon color={routeColor === "Admins" ? "primary" :"action"}/>} />
        </List>
        <Divider />
        <List>
          {role === "Super"
            ?<ListItemLink to="/Console" primary="Console" className={routeColor === "Console" ? classes.text : ""} icon={<GamesIcon color={routeColor === "Console" ? "primary" :"action"}/>} />
            :null
          }
          <ListItemLink to="/Setting" primary="Setting" className={routeColor === "Setting" ? classes.text : ""} icon={<SettingsIcon color={routeColor === "Setting" ? "primary" :"action"}/>} />
          <ListItem button key="Login" onClick={handleDialogClickOpen}>
              <ListItemIcon><ExitToAppIcon /></ListItemIcon>  
              <ListItemText primary="Login"/>
            </ListItem>
        </List>
      </Drawer>
      {renderMobileMenu}
      <Dialog open={dialogOpen} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
        <Alert
          severity="warning"
          action={
            <Button variant="contained" color="secondary" size="small" onClick={()=>{
                dispatch(logout())
              }}>
              Confirm
            </Button>
          }
        >
          Please confirm to logout.
        </Alert>
      </Dialog>
      <Switch>
        <Route path="/" exact={true} component={Dashboard} />
        <Route path="/Dashboard"  component={Dashboard} />
        <Route path="/Reports" component={Reports} />
        <Route path="/Users/:id?" component={Users} />
        <Route path="/Admins" component={Admins} />
        <Route path="/Setting" component={Setting} />
        <Route path="/Console" component={Console} />
        <Route component={NoMatch} />
      </Switch>
    </div>
  );
}