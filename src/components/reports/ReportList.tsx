import React, { useEffect } from 'react';
import clsx from 'clsx';
import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import ReplyIcon from '@material-ui/icons/Reply';
import { Box, Button, Collapse, Dialog } from '@material-ui/core';
import { getReportDataThunk, restoreMultiRepliedStatusThunk, updateMultiRepliedStatusThunk, updateReadStatusThunk, updateRepliedStatusThunk } from '../../redux/report/thunk';
import RestoreIcon from '@material-ui/icons/Restore';
import { Alert } from '@material-ui/lab';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

interface Data {
  reportID: number,
  reporterID: number,
  reporter: string,
  email: string,
  category: string,
  targetID: number,
  content: string,
  isRead: string,
  isReplied: string,
  replied_by: string,
  date: string
}


function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

type Order = 'asc' | 'desc';

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key,
): (a: { [key in Key]: number | string }, b: { [key in Key]: number | string }) => number {
  return order === 'asc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface HeadCell {
  disablePadding: boolean;
  id: keyof Data;
  label: string;
  numeric: boolean;
}

const headCells: HeadCell[] = [
  { id: 'reportID', numeric: true, disablePadding: true, label: 'Report ID' },
  { id: 'email', numeric: false, disablePadding: false, label: 'Email' },
  { id: 'category', numeric: false, disablePadding: false, label: 'Category' },
  { id: 'date', numeric: false, disablePadding: false, label: 'Date' },
  { id: 'isReplied', numeric: false, disablePadding: false, label: 'Reply' },
  { id: 'replied_by', numeric: false, disablePadding: false, label: 'Last read / Replied_by' },
];

interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  numSelected: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof Data) => void;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  order: Order;
  orderBy: string;
  rowCount: number;
}



const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(1),
    },
    highlight:
      theme.palette.type === 'light'
        ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
        : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
    title: {
      flex: '1 1 100%',
    },
  }),
);

interface EnhancedTableToolbarProps {
  numSelected: number;
}



const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
      backgroundColor: "lightgrey"
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
    read: {
      backgroundColor: "gainsboro"
    },
    unread: {
      backgroundColor: "white"
    }
  }),
);

export default function EnhancedTable() {
  const classes = useStyles();
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof Data>('reportID');
  const [selected, setSelected] = React.useState<number[]>([]);
  const [page, setPage] = React.useState(0);
  const [dense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [collapseOpen, setCollapseOpen] = React.useState(new Array(rowsPerPage).fill(false));
  const dispatch = useDispatch();
  const [dialogOpen, setDialogOpen] = React.useState(false);
  const [replyData, setReplyData] = React.useState({id: 0, isReplied: ""})

  const keyword = useSelector((state: IRootState) => state.report.reportSearchKeyword);
  const rowsWithoutSearch = useSelector((state: IRootState) => state.report.reportData);
  const rowsAfterSearch = useSelector((state: IRootState) => state.report.reportDataAfterSearch);
  const currentAdmin = useSelector((state: IRootState) => state.login.isLogin === false ? null :state.login.currentAdmin);

  let rows = rowsWithoutSearch.slice();
  if (keyword !== "") {
    rows = rowsAfterSearch.slice()
  }

  useEffect(()=>{
    dispatch(getReportDataThunk())
  }, [dispatch])

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof Data) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (selected.length === 0) {
      const newSelecteds = rows.map((n) => n.reportID);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, reportID: number) => {
    const selectedIndex = selected.indexOf(reportID);
    let newSelected: number[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, reportID);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    setSelected(newSelected);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const isSelected = (name: number) => selected.indexOf(name) !== -1;

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  function openReportInfo(index: number, reportID: number, replied_by: string, isReplied: string) {
    if (collapseOpen[index] === false) {
      let newArray = [...collapseOpen]
      newArray[index] = true
      setCollapseOpen(newArray)
    } else {
      let newArray = [...collapseOpen]
      newArray[index] = false
      setCollapseOpen(newArray)
    }
    if (isReplied === 'yes') { return }
    if (replied_by !== "null" || parseInt(replied_by.slice((replied_by.search(":"))+2)) === currentAdmin?.adminID ) { return }
    dispatch(updateReadStatusThunk(reportID))
  }

  function handleClickReply(id: number, isReplied: string) {
    setReplyData({id: id, isReplied: isReplied})
    setDialogOpen(true);
  }

  const confirmUpdateReply = () => {
    setDialogOpen(false);
    setTimeout(()=>{
      setReplyData({id: 0, isReplied: ""})
    }, 500)
    dispatch(updateRepliedStatusThunk(replyData.isReplied, replyData.id))
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
    setTimeout(()=>{
      setReplyData({id: 0, isReplied: ""})
    }, 500)
  };

  const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
    const classes = useToolbarStyles();
    const { numSelected } = props;

    function clickMultiReply() {
      let newSelected = []
      for (let i = 0; i < selected.length; i++) {
        if (rows[selected[i] - 1].isReplied === "no") {
          newSelected.push(rows[selected[i] - 1].reportID)
        }
      }
      if (newSelected.length === 0) { setSelected([]); return }
      setSelected([])
      dispatch(updateMultiRepliedStatusThunk(newSelected))
    }

    function clickMultiNotReply() {
      let newSelected = []
      for (let i = 0; i < selected.length; i++) {
        if (rows[selected[i] - 1].isReplied === "yes") {
          newSelected.push(rows[selected[i] - 1].reportID)
        }
      }
      if (newSelected.length === 0) { setSelected([]); return }
      setSelected([])
      dispatch(restoreMultiRepliedStatusThunk(newSelected))
    }

    return (
      <Toolbar
        className={clsx(classes.root, {
          [classes.highlight]: numSelected > 0,
        })}
      >
        {numSelected > 0 ? (
          <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
            {numSelected} selected
          </Typography>
        ) : (
          <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
            List of Reports
          </Typography>
        )}
        {numSelected > 0 ? (
          <>
            <Tooltip title="Replied">
              <IconButton aria-label="replied" onClick={clickMultiReply}>
                <ReplyIcon />
              </IconButton>
            </Tooltip>
            {currentAdmin?.role === "Super"
            ?<Tooltip title="Restore reply status">
              <IconButton aria-label="restore" onClick={clickMultiNotReply}>
                <RestoreIcon />
              </IconButton>
            </Tooltip>
            :""}
          </>
        ) : ""}
      </Toolbar>
    );
  };

  function EnhancedTableHead(props: EnhancedTableProps) {
    const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
    const createSortHandler = (property: keyof Data) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };
  
    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={currentAdmin?.role !== "Intern" ? onSelectAllClick : ()=>{}}
              inputProps={{ 'aria-label': 'select all desserts' }}
            />
          </TableCell>
          {headCells.map((headCell) => (
            <TableCell
              key={headCell.id}
              align="center"
              padding='none'
              sortDirection={orderBy === headCell.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === headCell.id}
                direction={orderBy === headCell.id ? order : 'asc'}
                onClick={createSortHandler(headCell.id)}
              >
                {headCell.label}
                {orderBy === headCell.id ? (
                  <span className={classes.visuallyHidden}>
                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                  </span>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
          <TableCell></TableCell>
        </TableRow>
      </TableHead>
    );
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <EnhancedTableToolbar numSelected={selected.length} />
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.reportID);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <>
                      <TableRow
                        hover
                        role="checkbox"
                        aria-checked={isItemSelected}
                        tabIndex={index}
                        key={row.reportID}
                        selected={isItemSelected}
                        className={row.isRead === "yes" ? classes.read : classes.unread}
                      >
                        <TableCell padding="checkbox">
                          <Checkbox
                            checked={isItemSelected}
                            inputProps={{ 'aria-labelledby': labelId }}
                            onClick={(event) => handleClick(event, row.reportID)}
                            disabled={currentAdmin?.role === "Intern"}
                          />
                        </TableCell>
                        <TableCell component="th" id={labelId} scope="row" padding="none" align="center" onClick={(event) => openReportInfo(index, row.reportID, row.replied_by, row.isReplied)}>
                          {row.reportID}
                        </TableCell>
                        <TableCell align="center" padding="none" onClick={(event) => openReportInfo(index, row.reportID, row.replied_by, row.isReplied)}>{row.email}</TableCell>
                        <TableCell align="center" padding="none" onClick={(event) => openReportInfo(index, row.reportID, row.replied_by, row.isReplied)}>{row.category}</TableCell>
                        <TableCell align="center" padding="none" onClick={(event) => openReportInfo(index, row.reportID, row.replied_by, row.isReplied)}>{row.date}</TableCell>
                        <TableCell align="center" padding="none">
                          <Checkbox
                            checked={row.isReplied !== "no"}
                            color='primary'
                            onClick={(event) => handleClickReply(row.reportID, row.isReplied)}
                            disabled={currentAdmin?.role === "Intern" ? true : row.isReplied === "yes" && currentAdmin?.role !== "Super" ? true : false}
                          />
                        </TableCell>
                        <TableCell align="center" padding="none" onClick={(event) => openReportInfo(index, row.reportID, row.replied_by, row.isReplied)}>{row.replied_by === "null" ? "" :row.replied_by}</TableCell>
                        <TableCell>
                          <IconButton aria-label="expand row" size="small" onClick={() => openReportInfo(index, row.reportID, row.replied_by, row.isReplied)}>
                            {collapseOpen[index] ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                          </IconButton>
                        </TableCell>
                      </TableRow>
                      <TableRow>
                        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                          <Collapse in={collapseOpen[index]} timeout="auto" unmountOnExit>
                            <Box>
                              <Table>
                                <TableBody>
                                  <TableRow>
                                    <TableCell style={{ paddingBottom: 5, paddingTop: 5 }}>
                                      Reportee:
                                    </TableCell>
                                    <TableCell style={{ paddingBottom: 5, paddingTop: 5 }}>
                                      {row.targetID}
                                    </TableCell>
                                  </TableRow>
                                  <TableRow>
                                    <TableCell  style={{ paddingBottom: 5, paddingTop: 5}}>
                                      Content:
                                    </TableCell>
                                    <TableCell  style={{ paddingBottom: 5, paddingTop: 5, height: 200 }}>
                                      {row.content}
                                    </TableCell>
                                  </TableRow>
                                </TableBody>
                              </Table>
                            </Box>
                          </Collapse>
                        </TableCell>
                      </TableRow>
                    </>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
      <Dialog open={dialogOpen} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
        <Alert
          severity="warning"
          action={
            <Button variant="contained" color="secondary" size="small" onClick={confirmUpdateReply}>
              Confirm
            </Button>
          }
        >
          {replyData.isReplied === "no" ? "Please confirm that the report was replied." : "Please be noted that the reply status of the report will be restored."}
        </Alert>
      </Dialog>
    </div>
  );
}