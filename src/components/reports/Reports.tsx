import { createStyles, makeStyles, Theme } from "@material-ui/core";
import ReportList from "./ReportList";
import ReportSearch from "./ReportSearch";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);


export default function Reports(){
  const classes = useStyles();

  return(
    <main className={classes.content}>
      <div className={classes.toolbar} />
        <ReportSearch />
        <ReportList />
    </main>
  )
}