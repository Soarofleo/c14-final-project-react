import Paper from '@material-ui/core/Paper';
import { createStyles, makeStyles, Theme, IconButton, CircularProgress, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import UserDataTypeSelection from './UserDataTypeSelection';
import { useEffect } from 'react';
import { getSubDataThunk, getUserTagAnalystDataThunk } from '../../redux/dashboard/thunk';
import ReactECharts from 'echarts-for-react';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { closeSubDataMode } from '../../redux/dashboard/action';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    paper: {
      padding: 20,
      position: 'relative',
      minWidth: 600,
    },
    btn: {
      position: 'absolute',
      zIndex: 1,
    }
  }),
);

const UserChart: React.FC = () => {
  
  const classes = useStyles();
  const data = useSelector((state: IRootState) => state.dashboard.userData)
  const subData = useSelector((state: IRootState) => state.dashboard.subData)
  const subDataForPlace = useSelector((state: IRootState) => state.dashboard.subDataForPlace)
  const title = useSelector((state: IRootState) => state.dashboard.userDataType)
  const subDataMode = useSelector((state: IRootState) => state.dashboard.subDataMode)
  const subDataType = useSelector((state: IRootState) => state.dashboard.subDataType)
  const [loading, setLoading] = useState(true)
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(getUserTagAnalystDataThunk())
  }, [dispatch])

  useEffect(()=>{
    setLoading(true)
    if (title === 'place'){
      setTimeout(()=>{
        setLoading(false)
      }, 500)
    }
  }, [title, subDataMode])

  let chartData;
  if (subDataMode === true){
    chartData = subData
  } else {
    chartData = data
  }

  let chartType = {
    userTag: "User Tag Class Analyst",
    userWishTag: "User Wish Tag Class Analyst",
    gender: "User Gender Analyst",
    age: "User Age Analyst",
    place: "User Schedule Place Analyst"
  }

  let chartTitle = chartType[title]
  if (subDataMode){
    if (title === "gender"){
      if (subDataType === "其他"){
        chartTitle = `User Analyst - other - Age`
      } else {
        chartTitle = `User Analyst - ${subDataType} - Age`
      }
    } else {
      chartTitle = `${chartType[title]} - ${subDataType}` 
    }
    if (title === "age"){
      chartTitle = `User Analyst - ${subDataType} - gender`
    }
  }
  const option = {
    title : {
      text: chartTitle,
      subtext: '',
      x:'center'
    },
    tooltip : {
      trigger: 'item',
      formatter: "{b} : {c} ({d}%)"
    },
    legend: {
      orient: 'horizon',
      left: 'right',
      data: chartData
    },
    series : [
      {
      name: `${subDataMode ? subDataType : title}`,
      type: 'pie',
      radius : '75%',
      center: ['40%', '55%'],
      data: chartData,
      itemStyle: {
        emphasis: {
        shadowBlur: 10,
        shadowOffsetX: 10,
        shadowColor: 'rgba(0, 0, 0, 0.5)'
        }
      }
      }
    ]
  };
  function onChartReady(echarts: any) {
    setTimeout(()=>{
      setLoading(false)
    }, 500)
  }
  function onChartClick(param: any, echarts: any) {
    if (subDataMode === true){return}
    let dataType = param.data.name
    if (param.data.name === "other"){dataType = "其他"}
    dispatch(getSubDataThunk(dataType, title))
  };
  function onChartLegendselectchanged(param: any, echarts: any) {
    console.log(param, echarts);
  };

  return (
    <>
      <Paper className={classes.paper} square >
        {loading === true
        ? <CircularProgress />
        : <>
          {subDataMode === false 
          ? <UserDataTypeSelection />
          : <div className={classes.btn}>
                <IconButton onClick={()=>dispatch(closeSubDataMode())}>
                  <ArrowBackIcon/>
                </IconButton>
            </div>
          }</>
        }
        {title === 'place' && subDataMode === true && loading === false
          ?<Table aria-label="simple table" style={{marginTop: 30}}>
            <TableHead>
              <TableRow>
                <TableCell align="right">Rank</TableCell>
                <TableCell align="right">Place id</TableCell>
                <TableCell align="right">Count</TableCell>
                <TableCell align="right">Location</TableCell>
                <TableCell align="right">Place name</TableCell>
                <TableCell align="right">Google place id</TableCell>
                <TableCell align="right">Address</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {subDataForPlace.map((item, index) => (
              <TableRow key={'subDataForPlace' + index}>
                <TableCell align="right">{index + 1}</TableCell>
                <TableCell align="right">{item.id}</TableCell>
                <TableCell align="right">{item.count}</TableCell>
                <TableCell align="right">{item.location.x + ', ' + item.location.y}</TableCell>
                <TableCell align="right">{item.name}</TableCell>
                <TableCell align="right">{item.place_id}</TableCell>
                <TableCell align="right">{item.vicinity}</TableCell>
              </TableRow>
            ))}
            </TableBody>
          </Table>
          :<ReactECharts
            option={option}
            style={loading === true ? {display: "none"} :{ height: "50vw" }}
            onChartReady={onChartReady}
            onEvents={{
              'click': onChartClick,
              'legendselectchanged': onChartLegendselectchanged
            }}
          />
        }
      </Paper>
    </>
  );
}

export default UserChart;
