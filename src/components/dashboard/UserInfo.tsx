import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import { SvgIcon } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { changeDataType } from '../../redux/dashboard/action';

const useStyles = makeStyles({
  root: {
    minWidth: 180,
    width: "23%",
    marginBottom: 12,
    marginRight: 6,
    marginLeft: 6,
  },
  darkModeRoot: {
    minWidth: 180,
    width: "23%",
    marginBottom: 12,
    marginRight: 6,
    marginLeft: 6,
    backgroundColor: "grey",
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    color: 'yellowgreen',
    display: 'flex',
  },
  neg: {
    color: 'red',
    display: 'flex',
  }
});

type Props = {
  // onCardClick: (type: string) => void;
  cardName: "dailyUser" | "dailyRegistration" | "dailyMatch" | "dailyConsumption";
}

export default function UserInfo(props: Props) {
  const classes = useStyles();
  const statData = useSelector((state: IRootState) => state.dashboard.statData)
  const dispatch = useDispatch();
  const themeColor = useSelector((state: IRootState) => state.login.isLogin === true ? state.login.currentAdmin.theme : "lightBlue");
  
  let change = 0;
  let percentage = "0%";
  let data = statData.dailyUser;
  let cardTitle;

  if (props.cardName === "dailyUser"){
    data = statData.dailyUser;
    cardTitle = "Daily Active Users"
  } else if (props.cardName === "dailyRegistration"){
    data = statData.dailyRegistration;
    cardTitle = "New Registration"
  } else if (props.cardName === "dailyMatch"){
    data = statData.dailyMatch
    cardTitle = "Daily Match"
  } else if (props.cardName === "dailyConsumption"){
    data = statData.dailyConsumption
    cardTitle = "Daily Coin Expenses"
  }

  if (data.length > 1 && data[0] !== null){
    if (data[1] !== null && data[1] !== 0 && data[0] !== 0){
      change = (data[0] - data[1])
      percentage = Math.round((data[0] - data[1]) * 10000/data[1])/100 + '%'
    } else if (data[1] === 0){
      change = (data[0] - data[1])
      percentage = data[0].toString()
    } else if (data[0] === 0 && data[1] !== null){
      change = (data[0] - data[1])
      percentage = (data[1]*(-1)).toString()
    }
  }

  function viewDailyUser(){
    dispatch(changeDataType(props.cardName))
  }

  return (
    <Card className={themeColor === "darkMode" ? classes.darkModeRoot : classes.root} variant="outlined">
      <CardContent>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          {cardTitle}
        </Typography>
        <Typography variant="h5" component="h2">
          {data[0]}
        </Typography>
        <Typography className={change >= 0 ?classes.pos :classes.neg} color="textSecondary">
          {change >= 0 ?<SvgIcon component={ArrowUpwardIcon}/> :<SvgIcon component={ArrowDownwardIcon}/>}
          {percentage}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" onClick={viewDailyUser}>View More</Button>
      </CardActions>
    </Card>
  );
}