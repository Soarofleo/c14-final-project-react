import { createStyles, FormControl, makeStyles, Select, Theme } from '@material-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { changePeriod } from '../../redux/dashboard/action';


const useStyles = makeStyles((theme: Theme) =>
createStyles({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}),
);

export default function PeriodSelection (){

  const classes = useStyles();
  const dispatch = useDispatch();
  
  const handleChange = (event: React.ChangeEvent<{ value: any }>) => {
    let period = parseInt(event.target.value)
      dispatch(changePeriod(period))
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <Select
        native
        onChange={handleChange}
        placeholder="Last 7 Days"
      >
        <option value={7}>Last 7 Days</option>
        <option value={30}>Last 30 Days</option>
        <option value={90}>Last 3 months</option>
        <option value={365}>Last one year</option>
      </Select>
    </FormControl>
  )
}