import { createStyles, FormControl, makeStyles, Select, Theme } from '@material-ui/core';
import React from 'react';
import { useDispatch } from 'react-redux';
import { changeType } from '../../redux/dashboard/action';


const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
  }),
);

export default function ChartSelection (){

  const classes = useStyles();
  const dispatch = useDispatch();

  const handleChange = (event: React.ChangeEvent<{ name?: string; value: any }>) => {
    let num = parseInt(event.target.value)
    if (num === 0){
      dispatch(changeType("Bar"))
    } else if (num === 1){
      dispatch(changeType("Spline"))
    } else {
      dispatch(changeType("Pie"))
    }
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <Select
        native
        onChange={handleChange}
        placeholder="Bar Chart"
      >
        <option value={0}>Bar Chart</option>
        <option value={1}>Curve Chart</option>
      </Select>
    </FormControl>
  )
}