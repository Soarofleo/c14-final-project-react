import { createStyles, makeStyles, Theme, FormControl, InputLabel, Select } from '@material-ui/core';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { changeUserDataTypeThunk } from '../../redux/dashboard/thunk';
import { IRootState } from '../../redux/store';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
  }),
);

export default function UserDataTypeSelection (){
  const userDataType = useSelector((state: IRootState) => state.dashboard.userDataType)
  const classes = useStyles();
  const dispatch = useDispatch();
  
  useEffect(()=>{
    dispatch(changeUserDataTypeThunk(userDataType))
  }, [dispatch, userDataType])

  const handleChange = (event: React.ChangeEvent<{ name?: string; value: any }>) => {
    dispatch(changeUserDataTypeThunk(event.target.value))
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel shrink>User Analyst</InputLabel>
      <Select
        value={userDataType}
        onChange={handleChange}
        label="User Analyst"
        native
      >
        <option value={"userTag"}>User Tag Class Analyst</option>
        <option value={"userWishTag"}>User Wish Tag Class Analyst</option>
        <option value={"gender"}>User Gender Analyst</option>
        <option value={"age"}>User Age Analyst</option>
        <option value={"place"}>User Schedule Place Analyst</option>
      </Select>
    </FormControl>
  )
}