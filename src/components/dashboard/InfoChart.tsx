import Paper from '@material-ui/core/Paper';
import {
  Chart,
  BarSeries,
  Title,
  ArgumentAxis,
  ValueAxis,
  Tooltip,
} from '@devexpress/dx-react-chart-material-ui';
import { EventTracker, SplineSeries } from '@devexpress/dx-react-chart';
import dateConversion from '../utility/date'
import { useSelector } from 'react-redux';
import { IRootState } from '../../redux/store';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  darkMode: {
    backgroundColor: "grey",
    color: "white"
  }
});

export default function InfoChart(){

  const chartType = useSelector((state: IRootState) => state.dashboard.chartType)
  const statData = useSelector((state: IRootState) => state.dashboard.statData)
  const period = useSelector((state: IRootState) => state.dashboard.period)
  const statDataType = useSelector((state: IRootState) => state.dashboard.statDataType)
  const themeColor = useSelector((state: IRootState) => state.login.isLogin === true ? state.login.currentAdmin.theme : "lightBlue");
  const classes = useStyles();

  let data: {date: string, population: number | null}[] = []
  let tenDayCounter: any = 0;
  let monthCounter: any = 0;
  let dataType = statData.dailyUser
  let title;

  if (statDataType === "dailyUser"){
    dataType = statData.dailyUser;
    title = "Active Users Data"
  } else if (statDataType === "dailyRegistration"){
    dataType = statData.dailyRegistration
    title = "New Registration Data"
  } else if (statDataType === "dailyMatch"){
    dataType = statData.dailyMatch
    title = "Match Data"
  } else if (statDataType === "dailyConsumption"){
    dataType = statData.dailyConsumption
    title = "Coin Expenses Data"
  }

  for (let i = 0; i < period; i++){
    if (period === 7){
      let day = dateConversion(i)
      data.push({date: day, population: dataType[i]})
    } else if (period === 30){
      let day = dateConversion(i)
      data.push({date: day.slice(-2), population: dataType[i]})
    } else if (period === 90){
      let day = dateConversion(i)
      if (day.slice(-2) === "10" || day.slice(-2) === "20"){
        tenDayCounter = dataType[i]
      } else {
        tenDayCounter = tenDayCounter + dataType[i]
      }
      if (day.slice(-2) === "21"){
        data.push({date: "Late " + day.slice(0, 3), population: tenDayCounter})
        tenDayCounter = 0;
      } else if (day.slice(-2) === "11"){
        data.push({date: "Mid-" + day.slice(0, 3), population: tenDayCounter})
        tenDayCounter = 0;
      } else if (day.slice(-2) === "01"){
        data.push({date: "Early " + day.slice(0, 3), population: tenDayCounter})
        tenDayCounter = 0;
      }
    } else {
      let day = dateConversion(i)
      monthCounter = monthCounter + dataType[i]
      if (day.slice(-2) === "01"){
        data.push({date: day.slice(0, 3), population: monthCounter})
        monthCounter = 0;
      }
    }
  }

  return (
    <Paper className={themeColor === "darkMode" ? classes.darkMode : ""}>
      <Chart
        data={data}
      >
        <ArgumentAxis/>
        <ValueAxis showLine={true}/>

        {
          chartType === "Bar"
          ?<BarSeries
            valueField="population"
            argumentField="date"
          />
          :<SplineSeries
            valueField="population"
            argumentField="date"
          />
        }
        
        <Title
          text={title}
        />
        <EventTracker />
        <Tooltip />
      </Chart>
    </Paper>
  );
}
